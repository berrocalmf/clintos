<?php
class Database extends PDO
{
    // Datos para conexión con el servidor SQL Server:
    private $dbname = "Clintos8";
    private $host   = "FMBMDEV";
    private $user   = "sa";
    private $pass   = "Piloto12345";

    // "AgilisDMO", "ClintosPruebasSmall", "ClintosPruebas", "Clintos8"
    // "172.16.20.30", "SQLSERVERPC", "172.16.20.30\UNICIADEV2017"
    // "Piloto12345", "Cl1nt0s2015", "Un1c14Dev"

    // creamos la conexión a la base de datos (conexión SQL Server):
    public function __construct()
    {
        try {
            $this->dbh = parent::__construct("sqlsrv:Server=$this->host;Database=$this->dbname","$this->user","$this->pass"
                ,array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
        } catch(PDOException $e) {
            echo  $e->getMessage();
        }
    }

    // Ajuste: 20/02/2020 - Se cambió el servidor de bases de datos
    // Creamos la conexión a la base de datos (conexión MariaDB Server):
    // public function __construct() {
    //     try {

    //         $this->dbh = parent::__construct('mysql:host='.$this->host.';dbname='.$this->dbname.';charset=utf8',$this->user,$this->pass,
    //         [
    //             PDO::ATTR_EMULATE_PREPARES => false,
    //             PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
    //         ]
    //         );

    //     } catch(PDOException $e) {
    //         print "!Error¡: " .  $e->getMessage();
    //         die();
    //     }
    // }

    # función para cerrar una conexión pdo
    public function close_con() {
        $this->dbh = null;
    }

    # Evita la clonación del objeto:
    final protected function __clone() {}
    
    # Método destructor:
    function _destructor() {
        $this->dbh = null;
    }

}
