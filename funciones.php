<?php
	/**
	 * Archivo para generación de consecutivos y otras funciones de ayuda para el sistema en general
	 * Es el archivo que se demomina como un Helper del sistema
	 * 13/09/2019 17:13
	 * Ing. FMBM
	 */

	# Genera consecutivos por código de la aplicación (EXISTE UN SP QUE GENERA CONSECUTIVOS) 
	function fn_genConsecutivo($companiaIn, $sedeIn, $prefijoIn, $bodegaIn, $conn_In) {
		
		$consec = 0;

		$consulta  = "SELECT CONSECUTIVO FROM USCXS WHERE COMPANIA={$companiaIn} AND IDSEDE={$sedeIn} AND PREFIJO='{$prefijoIn}'";
		$res 	   = $conn_In->prepare($consulta);
		$res->execute();
		$resultado = $res->fetchall(PDO::FETCH_ASSOC);

		foreach($resultado as $row) {
			$consec = $row['CONSECUTIVO'];
		}	

		if ( $consec > 0 ) {
			$cons_update = "UPDATE USCXS SET CONSECUTIVO=CONSECUTIVO+1 WHERE COMPANIA={$companiaIn} AND IDSEDE={$sedeIn} AND PREFIJO='{$prefijoIn}'";
			$res_update  = $conn_In->prepare($cons_update);
            $res_up      = $res_update->execute();
		} else {
			$cons_insert  = "INSERT INTO USCXS (COMPANIA, IDSEDE, BODEGA, PREFIJO, CONSECUTIVO)";
			$cons_insert .= " VALUES ({$companiaIn}, {$sedeIn}, '01', '{$prefijoIn}', 1)";
			$res_insert   = $conn_In->prepare($cons_insert);
			$res_ins      = $res_insert->execute();
			$consec       = 1;
		}

		return $consec;
	}

	function fn_edadPersona($fe_nacimiento_in, $fe_final_in, $type_in=1, $date_format_in='') {

		$fe_nacimiento	= date_create($fe_nacimiento_in);
		$fe_final	   	= date_create($fe_final_in);
		$intervalo	   	= date_diff($fe_nacimiento,$fe_final);
		
		// El tipo 1 corresponde a fecha en formato texto, el tipo 2 corresponde al formato numérico
		if( $type_in == 1 ) {
			$edad = $intervalo->y . " años " .  $intervalo->m . " meses " . $intervalo->d . " días";
		} else {
			switch ($date_format_in) {
				case 'y':
					$edad = $intervalo->y + ($intervalo->m / 12) + ($intervalo->d / 365.25);
					break;
				case 'm':
					$edad = ($intervalo->y * 12) + $intervalo->m + (($intervalo->d + ($intervalo->h / 24)) / 30);
					break;
				case 'd':
					$edad = ($intervalo->y * 365.25) + ($intervalo->m * 30) + $intervalo->d + ($intervalo->h / 24);
					break;		
				case 'cy':
					$edad = $intervalo->y;
					break;
				case 'cm':
					$edad = $intervalo->m;
					break;
				case 'cd':
					$edad = $intervalo->d;
					break;	
				default: // Por defecto son años	
					$edad = $intervalo->y + ($intervalo->m / 12) + ($intervalo->d / 365.25);
			}
		}

		return $edad;

	}

	# Retorna la fecha del Servidor de Bases de Datos
	# Diseñado para SQL SERVER
	# 18.06.2021 - fberrocalm

	function fn_getFechaMotor($conn_In) {
		$consulta  = "SELECT CONVERT(DATETIME,SYSDATETIME(),121) AS FECHA";
		$res 	   = $conn_In->prepare($consulta);
		$res->execute();
		$resultado = $res->fetchall(PDO::FETCH_ASSOC);

		foreach($resultado as $row) {
			$fechaMotor = $row['FECHA'];
		}

		return $fechaMotor;
	}

	# Retorna la diferencia entre dos fechas, expresada de acuerdo al formato que 
	# especifique la App cliente
	# 18.06.2021 - fberrocalm
	# [ SOLO FUNCIONA LOS MINUTOS Y LOS SEGUNDOS PARA FECHAS PEQUEÑAS ]

	function fn_diferenciaFechas($fe_inicio_in, $fe_final_in, $date_format_in='') {

		$fe_inicio = date_create($fe_inicio_in);
		$fe_final  = date_create($fe_final_in);
		$intervalo = date_diff($fe_inicio,$fe_final);
		
		switch ($date_format_in) {
			case 'y':
				$dato = $intervalo->y;		// Año dos digitos
				break;
			case 'n':
				$dato = $intervalo->n;		// Mes si ceros iniciales
				break;
			case 'j':
				$dato = $intervalo->j;		// Día sin ceros iniciales
				break;		
			case 'g':
				$dato = $intervalo->g;		// Hora sin ceros iniciales
				break;
			case 'i':
				$dato = $intervalo->i;		// Minutos con ceros iniciales
				break;
			case 's':
				$dato = $intervalo->s;		// Segundos con ceros iniciales
				break;
			case 'u':
				$dato = $intervalo->u;		// Microsegundos con ceros iniciales
				break;			
			default: // Por defecto son años	
				$dato = $intervalo->y + ($intervalo->m / 12) + ($intervalo->d / 365.25);
		}

		return $dato;

	}

	// if (!function_exists('fn_minutos_transcurridos')) {
	function fn_minutos_transcurridos ($fe_inicio_in, $fe_final_in) {
		$fecha_i = $fe_inicio_in;
		$fecha_f = $fe_final_in;

		$minutos = (strtotime($fecha_i) - strtotime($fecha_f)) / 60;
		$minutos = abs($minutos); 
		$minutos = floor($minutos);
		
		return $minutos;
	}
	// }
?>
