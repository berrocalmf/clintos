<?php
    include_once 'database.class.php';
    include_once 'funciones.php';

    // Este es un controlador (AFI)

    class Afiliado
    {
        private $idafiliado;
        private $conn;

        public function __construct()
        {
            $this->conn=new Database();
        }

        # Procedimiento que se conecta con SQL Server:

        # Retorna el nombre de un paciente como un único dato (Controlador pacientes)
        function get_Nombre($idPaciente){

            $consulta  = "SELECT CONCAT(COALESCE(x.PNOMBRE,''),' ',COALESCE(x.SNOMBRE,''),' ',COALESCE(x.PAPELLIDO,''),' ',COALESCE(x.SAPELLIDO,''),' ')";
            $consulta .= " AS Nombre FROM AFI x WHERE x.IDAFILIADO='" . $idPaciente . "'";

            $sth = $this->conn->prepare($consulta);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);

            return $result['Nombre'];
        }

        # Valida si el afiliado está registrado en la base de datos
        function existePaciente($idPaciente){
            $consulta = "SELECT * FROM AFI WHERE IDAFILIADO='" . $idPaciente . "'";
            $sth = $this->conn->prepare($consulta);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);

            return $result;
        }

        # Retorna la info completa del paciente como un único dato (Controlador pacientes)
        function get_infoPaciente($idPaciente){

            $consulta = "SELECT CONCAT(TIPO_DOC, ' - ', DOCIDAFILIADO, ' ' , COALESCE(PNOMBRE,''),' ',COALESCE(SNOMBRE,''),";
            $consulta .= "' ',COALESCE(PAPELLIDO,''),' ',COALESCE(SAPELLIDO,''),' ')";
            $consulta .= " AS Nombre FROM AFI WHERE IDAFILIADO='" . $idPaciente . "'";
            
            $sth = $this->conn->prepare($consulta);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);

            return $result['Nombre'];
        }

        # Retorma el número de documento de un paciente
        function get_nodocumentoafi ($idPaciente) {
            $consulta = "SELECT x.DOCIDAFILIADO AS nodocumento FROM AFI x WHERE x.IDAFILIADO='{$idPaciente}'";
            
            $sth = $this->conn->prepare($consulta);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);

            return $result['nodocumento'];                
        }

        # Retorna la edad de un afiliado (Controlador pacientes)
        function get_Edad($idPaciente){
            $consulta = "SELECT x.FNACIMIENTO FROM AFI x WHERE x.IDAFILIADO='" . $idPaciente . "' AND x.FNACIMIENTO IS NOT NULL";
            $edad     = "";

            $sth = $this->conn->prepare($consulta);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);

            if (is_array($result)) {
                $fechanac = $result['FNACIMIENTO'];
                $fechaact = date("Y-m-d");    
                $edad = fn_edadPersona($fechanac, $fechaact);
            } else {
                $edad = "Fecha de nacimineto no definida";
            }

            return $edad;
        }

        # Retorna la edad del paciente según la fecha de la historia clínica consultada
        public function get_EdadHC($idPaciente, $hcpid) {
            $consulta = "SELECT x.FNACIMIENTO FROM AFI x WHERE x.IDAFILIADO='" . $idPaciente . "' AND x.FNACIMIENTO IS NOT NULL";
            $edad     = "";

            $sth = $this->conn->prepare($consulta);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);

            if (is_array($result)) {
                $fechanac = $result['FNACIMIENTO'];
                $fechahc  = $this->get_fechaHc($hcpid);

                if ($fechahc == null) {
                    $edad = "Fecha de historia no definida";
                } else {
                    $edad = fn_edadPersona($fechanac, $fechahc);
                }    
            } else {
                $edad = "Fecha de nacimineto no definida";
            }

            return $edad;

        }

        # Retorna la edad de un afiliado en años(Controlador pacientes)
        function get_EdadAnios($idPaciente){
            $consulta = "SELECT FNACIMIENTO FROM AFI WHERE IDAFILIADO='{$idPaciente}'";

            $sth = $this->conn->prepare($consulta);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);

            $fechanac = $result['FNACIMIENTO'];
            $fechaact = date("Y-m-d");
            return round(fn_edadPersona($fechanac, $fechaact, 2, 'y'));
        }

        # Consultar la Administradora de un Paciente (Controlador pacientes)
        function get_Asegurador($idafiliado){
            $consulta = "SELECT p.IDADMINISTRADORA as idTercero FROM AFI p WHERE p.IDAFILIADO='".$idafiliado."' AND p.IDADMINISTRADORA IS NOT null";

            $sth = $this->conn->prepare($consulta);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);
            return $result['idTercero'];
        }

        # Consultar la info completa de la Administradora de un Paciente (Controlador pacientes)
        function get_infoAsegurador($idafiliado){
            $aseguradora="";

            $consulta  = "SELECT CONCAT(p.IDADMINISTRADORA, ' - ', t.RAZONSOCIAL) as idTercero ";
            $consulta .= "FROM AFI p ";
            $consulta .= "JOIN TER t ON t.IDTERCERO=p.IDADMINISTRADORA ";
            $consulta .= "WHERE p.IDAFILIADO='{$idafiliado}' AND p.IDADMINISTRADORA IS NOT null";

            $sth = $this->conn->prepare($consulta);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);

            if (is_array($result)) {
                if (null == $result['idTercero']) {
                    $aseguradora = "Sin asegurador definido";
                } else {
                    if ("" == $result['idTercero']) {
                        $aseguradora = "Sin asegurador definido";
                    } else {
                        $aseguradora = $result['idTercero'];
                    }
                }
            } else {
                $aseguradora = "Sin asegurador definido";
            }

            return $aseguradora;
        }

        # Retorna el nombre del paciente de una historia clínica (Controlador historias)      [ ??? ]
        function get_NombreHCA($idHclinica){

            $consulta = "SELECT CONCAT(COALESCE(x.PNOMBRE,''),' ',COALESCE(SNOMBRE,''),' ',COALESCE(PAPELLIDO,''),' ',
                        COALESCE(SAPELLIDO,'')) AS Nombre FROM HCA y JOIN AFI x ON y.IDAFILIADO=x.IDAFILIADO 
                        WHERE y.CONSECUTIVO='" . $idHclinica . "'";

            $sth = $this->conn->prepare($consulta);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);

            return $result['Nombre'];
        }

        # Retorna la fecha de la historia clínica
        public function get_fechaHc($idHclinica) {

            $consulta = "SELECT y.FECHA FROM HCA y WHERE y.CONSECUTIVO='{$idHclinica}'";

            $sth = $this->conn->prepare($consulta);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);

            if (is_array($result)) {
                $fecha = $result['FECHA'];
            } else {
                $fecha = null;
            }

            return $fecha;
        }

        # Obtiene el nombre de la Plantilla de Historia Clínica (Controlador plantillas)
        # @idPlantilla = Consecutivo de la historia clínica
        function get_Plantilla($idPlantilla){

            $consulta = "SELECT x.DESCPLANTILLA FROM HCA y JOIN MPL x ON y.CLASEPLANTILLA=x.CLASEPLANTILLA WHERE y.CONSECUTIVO='{$idPlantilla}'";

            $sth = $this->conn->prepare($consulta);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);

            return $result['DESCPLANTILLA'];
        }

        # Retorna la clase de plantilla de un registro de historia clínica (Controlador historias)
        function get_ClasePlantilla($idHclinica){

            $consulta = "SELECT CLASEPLANTILLA FROM HCA WHERE HCA.CONSECUTIVO='{$idHclinica}'";
            
            $sth = $this->conn->prepare($consulta);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);

            return $result['CLASEPLANTILLA'];
        }

        # Function: Retorna el id de afiliado de la H. Clínica
        function get_idAfiliado($idHclinica){

            $consulta = "SELECT IDAFILIADO FROM HCA WHERE HCA.CONSECUTIVO='{$idHclinica}'";
            
            $sth = $this->conn->prepare($consulta);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);

            return $result['IDAFILIADO'];
        }

        # Function: Retorna el valor del tiempo permitido para edición de la H. Clínica
        function get_LAPSOMODIFICAHC($clasePlantilla){

            $consulta = "SELECT LAPSOMODIFICAHC FROM MPL WHERE MPL.CLASEPLANTILLA='{$clasePlantilla}'";
            
            $sth = $this->conn->prepare($consulta);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);

            return $result['LAPSOMODIFICAHC'];
        }

    }

?>    
