'use strict'

function cargarTablas(idSeccion, accion, url, idAfiliado) {

	if(idSeccion=="") {
		alert("Sección no especificado.");
		return;
	}

	if(accion=="") {
		alert("Acción no especificada.");
		return;
	}

	if(url=="") {
		alert("Archivo PHP no especificado.");
		return;
	}

	if(idAfiliado=="" || idAfiliado==0) {
		alert("Id. de Afiliado no especificado.");
		return;
	}
	
	$.ajax(
    {
    	async: true,
        url: url,
        type: "POST",
        data: {idAfiliado: idAfiliado, accion: accion},
        cache: true
    }).done(function(pagina){
        $("#"+idSeccion).html(pagina);
    });
}
