$(document).ready(function(){

    // Capturamos elementos de DOM:

    var label_lista = $('#lb_list_plt');                
    var lista       = $('#list_plt');
    var crear       = $('#mk_hc');
    var claseplt    = '';
    var edt_idafi   = $('#txtCodigoAfi');
    var id_paciente = '';
    var idPlantilla = '';

    // Clic boton nueva historia clínica:

    $('#new_hc').click(function(){

        id_paciente = $('#txtCodigoAfi').val().trim();
        ateid       = $('#txtAteId').val().trim();

        var pagina  = "./win_hcaphp.php";
        pagina += "?pacid="+id_paciente+"&ateid="+ateid;

        window.open(pagina,'_blank');
    });

});