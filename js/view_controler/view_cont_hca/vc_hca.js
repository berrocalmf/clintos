// $(document).ready(function(){});
var valor_control;

// Se ejecuta cuando algún control recibe el foco

function ingreso_campo(id_campo) {
    valor_control = $('#'+id_campo).val();
}

// Función para guardar el valor de los campos una vez se realiza algún evento sobre ellos
// id_campo = EDT_hcadid_secuencia, tipo_campo = tipocampo, consecutivo = consecutivo hca

function save_valor(id_campo, tipo_campo, consecutivo) {

    // < Si las variables recibidas están vacías (Se cancela el proceso) >

    if( id_campo == "" || tipo_campo == "" || consecutivo == "") {            
        console.log('Valores vacíos');
        return;
    }

    // < Se toma el control y se carga en una variable >

    var arrCheckbox;
    var control = $('#'+id_campo);                                         

    // Control de tipo MultiCheck
    if (tipo_campo == 'MultiCheck') {
        var valor_campo = 0;

        if(control[0].checked) {
            valor_campo = 1;
        }

        $.ajax({
            async: true,
            url: "./controller/hca_cont/hcaDao.php",
            type: "POST",
            data: {hcpdid: id_campo, tipo_campo: tipo_campo, valor_campo: valor_campo, hcpid: consecutivo},
            error: function(){
                alert(
                    "No se actualizó el campo: " + id_campo + " - Consecutivo: " + consecutivo + "Valor: " + valor_campo
                );
            }
        });

        return;
    }

    // Control de tipo lista:
    if(tipo_campo == 'Lista') {

        var valor_campo = control.val(); // < Tomamos el valor ingresado en el campo >

        if(valor_campo == "") {
            console.log("El valor del campo es vacío");
            return;
        }

        $.ajax({
            async: true,
            url: "./controller/hca_cont/hcaDao.php",
            type: "POST",
            data: {hcpdid: id_campo, tipo_campo: tipo_campo, valor_campo: valor_campo, hcpid: consecutivo},
            error: function(){
                alert(
                    "No se actualizó el campo: " + id_campo + " - Consecutivo: " + consecutivo + "Valor: " + valor_campo
                );
            }
        });

        return;

    }
    
    // Control de tipo hora
    if(tipo_campo == 'Hora') {

        // console.log('Campo tipo hora');

        var valor_campo = control.val(); // < Tomamos el valor ingresado en el campo > 
        
        const fecha = new Date();
        var f       = fecha.getFullYear()  + "/" + (fecha.getMonth() +1) + "/" +  fecha.getDate() + " " + valor_campo + ":00.000";
        
        valor_campo = f;

        // console.log('Valor del campo hora: ' + valor_campo);
        // console.log('Fecha: ' + f);
        // console.log('Valor del campo hora: ' + valor_campo);

        if(valor_campo=="") {
            console.log("El valor del campo es vacío");
            return;
        }

        $.ajax({
            async: true,
            url: "./controller/hca_cont/hcaDao.php",
            type: "POST",
            data: {hcpdid: id_campo, tipo_campo: tipo_campo, valor_campo: valor_campo, hcpid: consecutivo},
            error: function(){
                alert(
                    "No se actualizó el campo: " + id_campo + " - Consecutivo: " + consecutivo + "Valor: " + valor_campo
                );
            }
        });

        return;
    } 

    // Todos los demás tipos de controles
    if(control.val() !== valor_control) { 
        
        var valor_campo = control.val();
        
        if (valor_campo=="") {
            console.log("El valor del campo es vacío");
            return;
        }

        $.ajax({
            async: true,
            url: "./controller/hca_cont/hcaDao.php",
            type: "POST",
            data: {hcpdid: id_campo, tipo_campo: tipo_campo, valor_campo: valor_campo, hcpid: consecutivo},
            error: function(){
                alert(
                    "No se actualizó el campo: " + id_campo + " - Consecutivo: " + consecutivo + "Valor: " + valor_campo
                );
            }
        });

    }

}
