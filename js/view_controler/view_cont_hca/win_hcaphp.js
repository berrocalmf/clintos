// Capturamos los elementos de DOM que se desean manejar:

var label_lista = '';
var lista = '';
var crear = '';
var edt_idafi = ''; 
var idPlantilla = '';

var crearHc = () => {

// Declarations:

    lista = $('#list_plt');
    edt_idafi = $('#txt_pacid');
    edt_ateid = $('#txt_ateid');
    idhca = "";

    let claseplt = lista.val();              	// <-- Clase de Plantilla
    let id_paciente = edt_idafi.val();          // <-- Id. del Paciente
    let id_atencion = edt_ateid.val();          // <-- Id. Atención
    let id_hc = "";

// Validations:

    if(id_paciente=="") {
        alert("Msg1: Identificador de Paciente no válido.");
        return;
    }

    if(claseplt=="") {
        alert("Msg2: Debe seleccionar una plantilla.");
        return;
    }

// Actions:

    $.get(
        "./controller/hca_cont/hca_igu.php",
        { pacid: id_paciente, plahcid: claseplt, ateid: id_atencion },
        (response)=>{
            try {
				
                var items = JSON.parse(response);

                if (items.status == "success") {

                    document.getElementById('hclinica').innerHTML = items.response; // Set the returned page in the hclinica component
                    $("#txt_hcpid").val(items.id);

                    idhca = $('#txt_hcpid');    
                    id_hc = idhca.val();    

                    if (id_hc !== "") {
                        document.getElementById('dxp').disabled=false;
                        document.getElementById('seldx').disabled=false;
                    }

                } else {
                    document.getElementById('hclinica').innerHTML = items.response;
                }
				
            } catch (error) {
                document.getElementById('hclinica').innerHTML = response;
            }
			
            // document.getElementById('hclinica').innerHTML = response;
        }    
    );

}

$(function(){

    // [ Cierra la ventana de historias clínicas ]    -- VALIDAR QUE ESTE PROCESO FUNCIONA
    $('#guardahc').click(function(){

        // var ventana = window.self;
        // ventana.opener = window.self;
        // ventana.close();

        alert('Proceso del botón Guardar y Salir');
        var modals = $(".container div.modal");
	
        // console.log(modals[0].id);

        if (modals.length > 1) {
            // document.getElementById(modals[0].id).modal('hide');
            $("#"+modals[0].id).modal("hide").data('bs.modal', null);
        }

    });

    // [ Cierra la ventan de historias clínicas pero en edición debe estar inhabilitado ]
    // VALIDAR QUE ESTE PROCESO FUNCIONA
    $('#cancelahc').click(function(){
        // var ventana = window.self;
        // ventana.opener = window.self;
        // ventana.close();

        alert('Proceso del botón Cancelar');
        var modals = $(".container div.modal");
	
        // console.log(modals[0].id);
        if (modals.length > 1) {
            // document.getElementById(modals[0].id).modal('hide');
            $("#"+modals[0].id).modal("hide").data('bs.modal', null);
        }
    });

    // Boton search del Diagnnóstico
    $("#seldx").lookupbox({
        title: 'Seleccionar Diagnóstico',
        url: 'dx.php?descdx=',
        imgLoader: 'Loading...',
        width: 500,
        onItemSelected: function(data){
          $('input[name=dxp]').val(data.mdxid);
          document.getElementById('textdxp').innerHTML = "" + data.coddx + " " + data.nomdx + ""; 

          let hcp_id = $("#txt_hcpid").val();

          if (hcp_id > 0) {
            $.post(
                "hca_funciones.php",
                { accion: '1', mdxid: data.mdxid, hcpid: hcp_id },
                (response)=>{
                    if (response == 0) {
                        alert("Diagnóstico no actualizado");
                    }
                }    
            );
          }

        },
        tableHeader: ['ID Dx.', 'Cod. Dx', 'Diagnóstico']
    });

});    

$().ready(()=>{
    // console.log("La página fue cargada");
    if (document.getElementById('txt_hcpid')) {       // Si el elemento existe se realiza el procedimiento
        idPlantilla = $('#txt_hcpid').val().trim();
        action      = "";

        if (idPlantilla != "") {
            
            // Está definido el id, por lo cual se trata de una edición
            // if(!isNaN(idPlantilla)) {
                // Se llama al servidor para dibujar la interfaz de la Historia clínica
                
                action = $('#txt_action').val().trim();

                if (action == "") {
                    action = "edit";
                }

                $.get(
                    "./controller/hca_cont/hca_igu.php",
                    { hcpid: idPlantilla, accion: action },
                    (response)=>{
                        // document.getElementById('hclinica').innerHTML = response;
                        // Nuevo ------------------------------------------------------------
                        try {
                            var items = JSON.parse(response);

                            if (items.status == "success") {

                                document.getElementById('hclinica').innerHTML = items.response;
                                $("#txt_hcpid").val(items.id);

                                idhca = $('#txt_hcpid');    
                                id_hc = idhca.val();    

                                if (id_hc !== "") {
                                    document.getElementById('dxp').disabled=false;
                                    document.getElementById('seldx').disabled=false;
                                }

                            } else {
                                document.getElementById('hclinica').innerHTML = items.response;
                            }
                        } catch (error) {
                            document.getElementById('hclinica').innerHTML = response;
                        }   
                        // ------------------------------------------------------------------
                    }    
                );
            //}
        }
    }

});
