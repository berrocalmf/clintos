// Dadas la division que contiene todas las pestañas y la de la pestaña que se 
// quiere mostrar, la funcion oculta todas las pestañas a excepcion de esa.
function cambiarPestanna(pestannas,pestanna) {
    
    // console.log("Pestañas: 1 " + pestannas.id);
    // console.log("Pestaña: 1 " + pestanna.id);
    // Obtiene los elementos con los identificadores pasados.
    pestanna       = document.getElementById(pestanna.id);
    listaPestannas = document.getElementById(pestannas.id);

    //  console.log("Pestañas: 2 " + listaPestannas.id);
    //  console.log("Pestaña: 2 " + pestanna.id);
    // Obtiene las divisiones que tienen el contenido de las pestañas.
    cpestanna       = document.getElementById('c'+pestanna.id);
    listacPestannas = document.getElementById('contenido'+pestannas.id);
    
    // console.log("Pestaña 3 " + cpestanna.id);
    // console.log("Pestañas 3 " + listacPestannas.id);

    var elementoDiv = [listacPestannas.getElementsByTagName('div')[0],listacPestannas.getElementsByTagName('div')[1]
                ,listacPestannas.getElementsByTagName('div')[2]];
    // console.log(elementoDiv);

    var elementoLi = [listaPestannas.getElementsByTagName('li')[0],listaPestannas.getElementsByTagName('li')[1],
                listaPestannas.getElementsByTagName('li')[2]]
    // console.log(elementoLi);

    // Muestra el contenido de la pestaña pasada como parametro a la funcion, cambia el color de la pestaña y aumenta el padding para que tape el  
    // borde superior del contenido que esta juesto debajo y se vea de este modo que esta seleccionada.
    $(document).ready(function(){ 
        i=0;
        //while (typeof listacPestannas.getElementsByTagName('div')[i] != 'undefined'){
        while (typeof elementoDiv[i] != 'undefined'){
            $(elementoDiv[i]).css('display','none');
            $(elementoLi[i]).css('background','');
            $(elementoLi[i]).css('padding-bottom','');
            i += 1;
        }
        // Se muestra la pestaña seleccionada.
        $(cpestanna).css('display','');
        $(pestanna).css('background','#0062cc96');
        $(pestanna).css('padding-bottom','2px');
    });

}