<?php
    include_once("Afiliado.php");
    include_once("includes/header.php");

													// if( isset($_POST['idafiliado']) && isset($_POST['search']) ) {
    if( isset($_POST['idafiliado']) ) {

		
		// To Do:  AQUI SI O SI DEBE LLEGAR EL PARAMETRO IDAFILIADO


        // SUGERENCIA: CARGAR EN UN ARRAY LOCAL TODA LA INFORMACION DEL AFILIADO QUE NECESITAMOS DESPELGAR EN PANTALLA
        // UNA SOLA CONSULTA A LA BASE DE DATOS. (EL SISTEMA MANEJA TODA LA INFORMACIÓN DEL PACIENTE EN LOCAL O EN VARIABLES DE SESION)
        // O EN EL LOCA STORAGE

        $afi = new Afiliado(); // <CONTROLADOR DE AFILIADOS>
        $pacid = $_POST['idafiliado']; // <VARIABLE LOCAL ID DEL PACIENTE TRABAJADO>
        $conn = new Database();
		
    } else {
        echo "<div class='row'> Parámetros IDAFILIADO con valores incorrectos o sin dato. </ div>";
		return; // CORTA LA EJECUCIÓN DEL PROBRAMA
    }

?>

<?php include("includes/navigation.php"); ?>

<!-- start container -->

<!-- VISTA DE ATENCION DE PACIENTES -->

<div class="container"> 
    <div class="row">

        <!-- Datos del Afiliado -->
        <!-- <nav aria-label="breadcrumb" class="breadcrumb"> -->
        <div class="col-sm-8" style="background-color:lavender;" >
            <table>
                <tr><td><strong>Afiliado:  </strong></td><td><?php echo $afi->get_Nombre($pacid);     ?></td></tr>
                <tr><td><strong>Edad:      </strong></td><td><?php echo $afi->get_Edad($pacid);       ?></td></tr>
                <tr><td><strong>Asegurador:</strong></td><td><?php echo $afi->get_Asegurador($pacid); ?></td></tr>
            </table>
            <input type="hidden" id="txtCodigoAfi" name="txtCodigoAfi" value="<?=$pacid;?>">
            <input type="hidden" id="txtAteId"     name="txtAteId"     value="1">
        </div>    
        <!-- </nav> -->

        <div class="col-sm-8">
            
            <!-- Sistema de organización en pestañas -->
            <section id="seccionTablas" style="background-color: white;padding: 0px;" >

                <!-- Definición de pestañas -->
                <div id="pestanas">
                    <ul id=lista>
                        <li id="p1"><a href="javascript:cambiarPestanna(pestanas,p1);"><h6>Historia Cl&iacute;nica</h6></a></li>
                        <li id="p2"><a href="javascript:cambiarPestanna(pestanas,p2);"><h6>Ordenes M&eacute;dicas </h6></a></li>
                        <li id="p3"><a href="javascript:cambiarPestanna(pestanas,p3);"><h6>Citas                  </h6></a></li>
                    </ul>
                </div>

                <!-- <body onload="javascript:cambiarPestanna(pestanas,p1);"> -->
                <!-- Contenido de pestañas -->
                <div id="contenidopestanas">
                    <div id="cp1">
                        <!-- botón para activar controles de creación de HC -->
                        <button id="new_hc" type="button" class="btn btn-info">
                            <span class="fa fa-heartbeat"></span>
                            Nueva Historia
                        </button>

                        <!-- SE ELIMINA DE ESTA VENTANA LA SELECCIÓN DE PLANTILLA DE HISTORIA CLINICA -->
                        
                        <!-- <label id="lb_list_plt" for="list_plt">Plantilla:</label>
                        <select id="list_plt" name="list_plt" style="width: 600px; height: 36px;">
                            <option value=""></option> 
                            <?php 
                                // $qry_mpl = "SELECT x.plahcid, x.nomplantillahc FROM plahc x WHERE x.activa=1";
                                // $sth     = $conn->prepare($qry_mpl);
                                // $sth->execute();
                                // $res     = $sth->fetchAll(PDO::FETCH_ASSOC);
                                
                                // foreach ($res as $row) {
                                //    echo "<option value='".$row['plahcid']."'>".$row['nomplantillahc']."</option>";
                                //}
                            ?>
                        </select> -->
                        <!-- <button id="mk_hc" type="button" class="btn btn-info">
                            <span class="fa fa-plus-square"></span>
                            Crear
                        </button>  -->

                        <section id="seccionHClinica" style="background-color: white;padding: 10px;width: 1200px;">
                        </section>
                    </div>
                    <div id="cp2"> 
                        <section id="seccionAutorizaciones" style="background-color: white;padding: 10px;width: 1200px;">
                        </section>
                    </div>
                    <div id="cp3">
                        <section id="seccionCitas" style="background-color: white;padding: 10px;width: 1200px;">
                        </section>           
                    </div>
                </div>

            </section>

        </div>

    </div>
</div> 

<!-- end container -->

<script>
    cargarTablas("seccionHClinica",       "1", "tablasCE.php", $('#txtCodigoAfi').val());   
    cargarTablas("seccionAutorizaciones", "2", "tablasCE.php", $('#txtCodigoAfi').val());
    cargarTablas("seccionCitas",          "3", "tablasCE.php", $('#txtCodigoAfi').val());
	cambiarPestanna(pestanas,p1);
</script>

<script src="js/view_controler/view_cont_hca/vc_atencionCE.js" ></script>

<?php
    include("includes/footer.php");
?>
