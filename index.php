<?php include("includes/header.php"); ?>
<?php include("includes/navigation.php"); ?>

<!-- start container -->

<div class="container"> 
    <div class="row">
        
        <!-- action="<?php echo $_SERVER['PHP_SELF']; ?>" -->
        <!-- Esta instrucción se usa para referirse al mismo archivo como action del form -->
        <!-- value="<?php echo isset($_POST['idafiliado']) ? $_POST['idafiliado'] : ""; ?>" -->
        <!-- Con esta instrucción se asigna un valor a una caja de texto -->
        <!-- class="form-inline my-2 my-lg-0"  (Clase para el formulario)-->
        
        <form method="post" action="atencionCE.php">

            <label for="idafiliado">Par&aacute;metro Id. Paciente:</label>
            <br>
            <input class="form-control mr-sm-2" type="text" name="idafiliado" id="idafiliado" placeholder="Id. Paciente"
                   aria-label="Id. Paciente" value="">
            <br>
            <button class="btn btn-outline-success my-2 my-sm-0" name="search" id="search" type="submit">
                <span class="fa fa-user"></span>
                Par&aacute;metro
            </button>

        </form>
 
    </div>
</div> 

<!-- end container -->

<?php include("includes/footer.php"); ?>
