<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Prueba Consulta Externa</a>
    
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
            aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="fa fa-home"></span>
        Inicio
    </button>
    
    <div class="collapse navbar-collapse">
        <ul class="navbar-nav mr-auto">

            <!--
            <li class="nav-item active">
                <a class="nav-link" href="#">Inicio <span class="sr-only">(current)</span></a>
            </li>
            -->

            <li class="nav-item">
                <a class="nav-link" href="#">Link parte del menu de navegación de la página</a>
            </li>

            <!--
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false">Consulta Externa</a>
                <div class="dropdown-menu" aria-labelledby="dropdown01">
                    <a class="dropdown-item" href="#">Historias Clínicas</a>
                    <a class="dropdown-item" href="#">Citas Médicas</a>
                    <a class="dropdown-item" href="#">Autorizaciones</a>
                </div>
            </li> 
            -->

        </ul>

        <!--
        <form class="form-inline my-2 my-lg-0" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
            <input class="form-control mr-sm-2" type="text" name="idafiliado" placeholder="Id Afiliado"
                   aria-label="Par&aacute;metro" value="<?php echo isset($_POST['idafiliado']) ? $_POST['idafiliado'] : ""; ?>">
            <button class="btn btn-outline-success my-2 my-sm-0" name="search" type="submit">Par&aacute;metro</button>
        </form>
        -->

    </div>
</nav>