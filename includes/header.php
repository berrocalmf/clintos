<?php
    //ob_start();
?>
<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sistema de Atenci&oacute;n M&eacute;dica</title>

    <!-- Bootstrap core CSS -->
    <!-- bloqueado temporalmente
        <link href="css/bootstrap.min.css" rel="stylesheet">
    -->
    
    <link href="css/cssEstiloPestannas.css" rel="stylesheet">
    <link rel="stylesheet" href="css/jquery-ui.min.css">
    <link rel="stylesheet" href="css/lookupbox.css">

    <!-- Custom Fonts -->
    <!-- Comentada temporalmente
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    -->

    <!-- Prueba con librerías de PHPRunner -->
    <link href="css/phprunnercss/default.css" rel="stylesheet">
    <link href="css/phprunnercss/defaultRTL.css" rel="stylesheet">
    <link href="css/phprunnercss/editor.css" rel="stylesheet">
    <link href="css/phprunnercss/bootstrap/cerulean/normal/style.css" rel="stylesheet">
    <link href="css/phprunnercss/bs.css" rel="stylesheet">
    <link href="css/phprunnercss/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    
    <!-- <script src="js/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script> -->

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- <script type="text/javascript" src="js/cambiarPestanna.js"></script> -->
    <script src="./js/cargarTablas.js" defer></script>
    <script src="./js/cambiarPestanna.js" defer></script>

    <script src="js/jquery-ui.min.js"></script>
    <script src="js/jquery.lookupbox.js"></script>

    <!-- Custom styles for this template -->
</head>
<body>
