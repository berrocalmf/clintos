<!doctype html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">

		<title>Registro de Historia cl&iacute;nica</title>
	 
		<!-- Bootstrap core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/my_styles.css" rel="stylesheet">
		<link href="css/phprunnercss/font-awesome/css/font-awesome.min.css" rel="stylesheet">
		<link rel="stylesheet" href="css/jquery-ui.min.css">
		<link rel="stylesheet" href="css/lookupbox.css">

		<style>
			input:focus {                                           /* Color de fondo al ingresar al control de tipo input*/
				background-color: #fdfd96;
			}
			hr.style4 {                                             /* Estilo para separadores horzontales */
				border-top: 1px dotted #8c8b8b;
			}
		</style>

	</head>

	<body>

		<?php 
		
		// LINK A ARCHIVOS REQUERIDOS:
			
			include_once 'Afiliado.php';
			include_once 'funciones.php';									// include_once './controller/hca_cont/hca_controler.php'; COMENTADA
			
		// ------------------------------------------------------------------------------------------------------------------------------------------------------------------	

		// DECLARACION DE VARIABLES Y CONSTANTES:

			// Constantes:
			
			define('MAX_COLS', '2'); // <-- máximo número de columnas de la plantilla (Limita el layout a 2 columnas).  Se definirá en la configuracion de MPL

			// Variables:
			
			$conn      = new Database();				//$hca_cont  = new hca_controler(); COMENTADA
			$readonly  = "";
			$activadx  = "";
			$afi       = new Afiliado();
			$autofocus = '';
			$id_plt    = "";
			$plantilla = "";
			$editar    = 0;
			$cons_hc   = "";
			$pacid     = "";
			$edad_pac  = "";
			$action    = "";
			$id_ate    = "";
			
		// ------------------------------------------------------------------------------------------------------------------------------------------------------------------		

		// VALIDACIONES:

			// también se valida si se recibió como parámetro el id del Paciente:
			
			if (isset($_GET['pacid'])) {
				$pacid = $_GET['pacid'];

				if(!(is_array($afi->existePaciente($pacid)))) {         // <-- Se valida que el id de paciente sea válido
					echo "<div class='row'> Paciente no registrado. </ div>";
					return; // CANCELA LA EJECUCION DEL PROGRAMA
				}

			} else {
				echo "<div class='row'> Parámetros IDAFILIADO con valores incorrectos o sin dato. </ div>";
				return;		// CANCELA LA EJECUCION DEL PROGRAMA
			}


			// Valida si el dato hcpid está definido o no estpa definido (de esto depende si es nueva historia o si es edición)

			if (!isset($_GET['hcpid'])) { // CONSECUTIVO HCA
				$readonly = "";
				$activadx = "disabled";
				$editar = 0;
				$edad_pac = $afi->get_Edad($pacid);

				if (isset($_GET['ateid'])) {
					$id_ate = $_GET['ateid'];
				} else {
					$id_ate = "";
				}

			} else {
				$readonly = "disabled";
				$cons_hc = $_GET['hcpid']; // CONSECUTIVO HCA
				$editar = 1; 
				$edad_pac = $afi->get_EdadHC($pacid, $cons_hc);

				if (isset($_GET['action'])) {
					$action = $_GET['action'];
				} else {
					$action = "";
				}

				$plantilla = $afi->get_Plantilla($cons_hc);         // <-- Obtiene el nombre de la Plantilla
				$id_plt    = $afi->get_ClasePlantilla($cons_hc);    // <-- clase de la plantilla de Historia Clínica
			}
		
		// ------------------------------------------------------------------------------------------------------------------------------------------------------------------	

		// INTERAZ DE USUARIO
		
			// Datos del Paciente:  COMENTADO TEMPORALMENTE ...

			echo '<div class="container">';

				echo '<div class="row p-3 mb-2 bg-info text-white">';
					echo '<table border="0">';
						echo '<tr><td><strong>Afiliado: </strong></td><td>'.$afi->get_infoPaciente($pacid).'</td></tr>';
						echo '<tr><td><strong>Edad: </strong></td><td>'.$edad_pac.'</td></tr>';
						echo '<tr><td><strong>Administradora: </strong></td><td>'.$afi->get_infoAsegurador($pacid).'</td></tr>';
						echo '<tr><td><strong>Consecutivo: </strong></td><td>'.$cons_hc.'</td></tr>';
					echo '</table>';
					echo '<input type="hidden" id="txt_hcpid"  name="txt_hcpid" value="'  . $cons_hc . '">';
					echo '<input type="hidden" id="txt_pacid"  name="txt_pacid" value="'  . $pacid   . '">';
					echo '<input type="hidden" id="txt_action" name="txt_action" value="' . $action  . '">';
					echo '<input type="hidden" id="txt_ateid"  name="txt_ateid"  value="' . $id_ate  . '">';
				echo '</div>';

				// Bloque para la selección de diagnósticos

				echo '<div class="row p-3 mb-2 bg-info text-white" style="padding-top: 0px !important; padding-bottom: 0px !important;">';

					echo '<table border="0">';

						echo '<tr>';

							echo '<td>';

								echo '<div class="row p-3 mb-2 bg-info text-white" style="padding-bottom: 0px !important;">';
									echo '<label for="dxp" style="font-size: smaller;">Dx. principal</label> &nbsp;&nbsp;';
									echo '<input type="text" id="dxp" name="dxp" class="form-control" style="width: 90px; height: 30px; display: block;" ' . $activadx . '/> &nbsp;';
									echo '<button type="button" class="btn btn-dark" name="seldx"  id="seldx" style="height: 32px; display: block;" ' . $activadx . ' >'; 
										echo '...';
										// echo '<span class="fa fa-heartbeat"></span>';
									echo '</button>';
								echo '</div>';    
								echo '<label id="textdxp" style="font-size: small; padding-bottom: 0px !important;">Diagnóstico</label>';
							echo '</td>';

						echo '</tr>';

					echo '</table>';

				echo '</div>';

				// Listado de Plantillas de Historias Clínicas:    

				echo '<div class="row p-3 mb-2 bg-info text-white">';
					echo '<label id="lb_list_plt" for="list_plt" style="display: block;">Plantilla:&nbsp;&nbsp;</label>';
					echo '<select id="list_plt" name="list_plt" style="width: 600px; height: 36px; display: block;" ' . $readonly . '>';
						echo '<option value=""></option>';

							$qry_mpl = "SELECT x.CLASEPLANTILLA as plahcid, x.DESCPLANTILLA as nomplantillahc FROM MPL x WHERE x.ESTADO='Activo'";
							$sth     = $conn->prepare($qry_mpl);
							$sth->execute();
							$res     = $sth->fetchAll(PDO::FETCH_ASSOC);
							
							foreach ($res as $row) {
								if($id_plt === $row['plahcid']) {
									echo "<option value='".$row['plahcid']."' selected>".$row['nomplantillahc']."</option>";
								} else {
									echo "<option value='".$row['plahcid']."'>".$row['nomplantillahc']."</option>";
								}
							}

					echo '</select> &nbsp;&nbsp;';

					// Botón para crear la nueva HC
					echo '<button id="mk_hc" style="display: block;" type="button" class="btn btn-secondary" onclick="crearHc()" ' . $readonly . '>';
						echo '<span class="fa fa-plus-square"></span>';
						echo 'Crear';
					echo '</button>';
					echo '&nbsp;&nbsp;&nbsp;';
					echo '<button type="button" class="btn btn-primary" name="guardahc"  id="guardahc" >Guardar y Salir</button>';
					echo '&nbsp;&nbsp;&nbsp;';
					echo '<button type="button" class="btn btn-danger"  name="cancelahc" id="cancelahc">Cancelar</button>';

				echo '</div>';

			echo '</div>';
		?>

		<!-- Contenedor donde se mostrarán los campos de la historia clinica -->
		<div id="hclinica" class="container border border-primary rounded">
		</div>

		<!-- <script src="js/jquery-3.3.1.slim.min.js"></script> -->
		<script src="js/jquery-3.3.1.min.js"></script>
		<script src="js/popper.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/view_controler/view_cont_hca/vc_hca.js"></script>
		<script src="js/view_controler/view_cont_hca/win_hcaphp.js" ></script>

		<script src="js/jquery-ui.min.js"></script>
		<script src="js/jquery.lookupbox.js"></script>
		
	</body>

</html>
