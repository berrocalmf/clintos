<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Clintos</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <style>
        input:focus {
            background-color: #fdfd96;
        }

        hr {
            -moz-border-bottom-colors: none;
            -moz-border-image: none;
            -moz-border-left-colors: none;
            -moz-border-right-colors: none;
            -moz-border-top-colors: none;
            border-color: #EEEEEE -moz-use-text-color #FFFFFF;
            border-style: solid none;
            border-width: 1px 0;
            margin: 18px 0;
        }
    </style>
    <!-- Custom styles for this template -->

<body>

<?php
//include("database.class.php");
include("Afiliado.php");
if (!isset($_GET['consecutivo'])) {
    echo '<div class="alert alert-danger" role="alert"><strong>Debe ingresar un consecutivo</strong></div>';
    exit;
}
/* Connect using Windows Authentication. */
$conn = new Database();
$afi = new Afiliado();
$autofocus = '';

echo '
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><strong>Afiliado:</strong> ' . $afi->get_NombreHCA($_GET['consecutivo']) . '</li>
    <li class="breadcrumb-item"><strong>Consecutivo:</strong> ' . $_GET['consecutivo'] . '</li>
    <li class="breadcrumb-item active" aria-current="page"><strong>Plantilla: </strong>' . $afi->get_Plantilla($_GET['consecutivo']) . '</li>
  </ol>
</nav>';


$consulta = " SELECT a.FECHA,a.OMEDID,a.omecid,a.IDSERVICIO,a.CANTIDAD,a.VIA, a.APLICADA,a.OBSERVACION,a.FRECUENCIA,
	a.IDMEDIDAF,c.DESCRIPCION AS MEDIDAF, a.IUNIID,b.DESCRIPCION as UNIDAD,a.IDMEDICOORDENA as IDMEDICO,
	a.CONSECUTIVO, a.IDAFILIADO, a.DOSIS, a.DURACION, a.IDMEDIDAD, x.CONCENTRACION, om.SECUENCIA, me.NOMBRE AS MEDICO    
	from omed a 
	INNER JOIN OMEC om ON om.OMECID=a.OMECID 
	INNER JOIN MED me on me.IDMEDICO=a.IDMEDICOORDENA 
	LEFT JOIN IUNI b on b.IUNIID=a.IUNIID 
	LEFT JOIN OMECC c on c.OMECCID=a.IDMEDIDAF 
	LEFT JOIN SERPV x on x.IDSERVICIO=a.IDSERVICIO 
	
	WHERE 	a.CONSECUTIVO='" . $_GET['consecutivo'] . "' 
	ORDER BY om.SECUENCIA ";
//echo $consulta;
exit;
$sth = $conn->prepare($consulta);
$sth->execute();
$result = $sth->fetchall(PDO::FETCH_ASSOC);
echo '<div class="container">';
//echo '<table class="table">';
echo '<div class="row"><div class="col">&nbsp;</div> </div>';
echo '<div class="row">';
echo '<div class="col"><button type="button" class="btn btn-primary">Guardar y Salir</button>&nbsp;&nbsp;<button type="button" class="btn btn-danger">Cancelar</button></div>';
echo '<div class="row"><div class="col">&nbsp;</div> </div>';
echo '</div>';
foreach ($result as $row) {
    echo '<div class="row">';
    $fecha=$row['FECHA'];
    $fecha=$row['FECHA'];
    $fecha=$row['FECHA'];

    $tipocampo = $row['TIPOCAMPO'];
    $campo = $row['CAMPO'];
    $desccampo = $row['DESCCAMPO'];
    $secuencia = $row['SECUENCIA'];
    $alfanumerico = $row['ALFANUMERICO'];
    $fecha = $row['FECHA'];
    $memo = $row['MEMO'];
    $claseplantilla = $row['CLASEPLANTILLA'];
    $a = 'autofocus';

    switch ($tipocampo) {
        case "Alfanumerico":
            {
                echo '<div class="col"> <label>' . $desccampo . ' </label><input type="text" ' . $a . ' name="EDT_' . $campo . '_' . $secuencia . '" value="' . $alfanumerico . '" size="50" class="form-control" ></div>';
                break;
            }
        case "Memo":
            {
                echo '<div class="col"><label>' . $desccampo . ' </label><textarea ' . $a . ' name="TEXT_' . $campo . '_' . $secuencia . '" cols="60" rows="4" class="form-control">' . $memo . '</textarea></div>';
                break;
            }
        case "Fecha":
            {
                echo '<div class="col"><label>' . $desccampo . ' </label><input type="text" ' . $a . ' name="EDT_' . $campo . '_' . $secuencia . '" value="' . $fecha . '"  class="form-control" ></div>';
                break;
            }
        case "Lista":
            {
                echo '<div class="col"><label>' . $desccampo . ' </label><select ' . $a . ' name="EDT_' . $campo . '_' . $secuencia . '" class="form-control" >';
                $sqlLista = "SELECT VALORLISTA FROM MPLDL WHERE CLASEPLANTILLA='" . $claseplantilla . "' AND CAMPO='" . $campo . "' ";
                echo '<option value=""></option>';
                foreach ($conn->query($sqlLista) as $rowx) {

                    IF ($rowx['VALORLISTA'] == $alfanumerico) {
                        $sel = 'selected="selected"';
                    } else $sel = '';
                    echo '<option value="' . $rowx['VALORLISTA'] . '" ' . $sel . ' >' . $rowx['VALORLISTA'] . '</option>';
                }
                echo '</select></div>';
                break;
            }
    }
    echo '<hr>';
    if ($autofocus == '') {
        $autofocus = 'Y';
        $a = '';
    }
    echo '</div>';
}
//echo '</table>';
echo '<div class="row"><div class="col">&nbsp;</div> </div>';
echo '<div class="row">';
echo '<div class="col"><button type="button" class="btn btn-primary">Guardar y Salir</button>&nbsp;&nbsp;<button type="button" class="btn btn-danger">Cancelar</button></div>';
echo '</div>';
?>
<script src="js/jquery-3.3.1.slim.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>
