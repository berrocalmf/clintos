<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Historia clínica</title>
 
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/my_styles.css" rel="stylesheet">

    <!-- <style>
        input:focus {                                           /* Color de fondo al ingresar al control de tipo input*/
            background-color: #fdfd96;
        }
        hr.style4 {                                             /* Estilo para separadores horzontales */
            border-top: 1px dotted #8c8b8b;
        }
    </style> -->

</head>
<body>
    <?php
        include("Afiliado.php");
        include_once 'funciones.php';
        include_once './controller/hca_cont/hca_controler.php';
        
        define('MAX_COLS', '2'); // <-- máximo número de columnas de la plantilla (Limita el layout a 2 columnas).  Se definirá en la configuracion de MPL

        $conn     = new Database();
        $hca_cont = new hca_controler();

        // Change Log:
        // 23-08-2019: Se agrega el método onblur a los controles para realizar el guardado o actualización de los campos de la Historia en Edición
        // Si viene consecutivo es edición si no viene consecutivo es creación (se valida que se seleccionó la plantilla)

        if (!isset($_GET['hcpid'])) {

            if (!isset($_GET['claseplt'])){
                echo '<div class="alert alert-danger" role="alert"><strong>Valor: Clase de Plantilla no válido</strong></div>';
                exit;
            }

            if (!isset($_GET['id_paciente'])){
                echo '<div class="alert alert-danger" role="alert"><strong>Valor: Id. Paciente no válido</strong></div>';
                exit;
            }

            $plantilla   = $_GET['claseplt'];
            $id_afiliado = $_GET['id_paciente'];
            $nombre_host = gethostbyaddr($_SERVER['REMOTE_ADDR']); // <-- Nombre del Computador
            
            // Historia Clínica Nueva ...
            $cons_hc = '07' . str_pad(fn_genConsecutivo("01", "07", "@HC", "", $conn),8,"0",STR_PAD_LEFT); // <-- 1 - Se genera un nuevo consecutivo

            if(isset($cons_hc)) {

                // 2 - Se debe crear la HC ... de ser posible utilizando un SP

                $h_clinica = [
                    "CONSECUTIVO"        => $cons_hc,
                    "NOADMISION"         => "0100000001",
                    "IDAFILIADO"         => $id_afiliado,
                    "IDMEDICO"           => "1067874684",
                    "AFITTODID"          => 0,
                    "CLASEPLANTILLA"     => $plantilla,
                    "IDMEDICODILIGENCIA" => "1067874684",
                    "EDAD"               => "28 anios",
                    "IDAREA"             => "4105",
                    "SYS_ComputerName"   => $nombre_host,
                    "PROCEDENCIA"        => "QX",
                    "GLASGOW"            => "Alerta",
                    "CLASE"              => "HC",
                    "ESTADO"             => "Activa",
                    "IDSEDE"             => "07"    
                ];

                // Creación de la HC:
                if($hca_cont->fn_crear_registro_hc($conn,$h_clinica) == true) {

                    // 3. Creación de campos y de valores de lista:
                    $dataHc = [
                        "CONSECUTIVO"    => $cons_hc,
                        "CLASEPLANTILLA" => $plantilla,
                        "IDAFILIADO"     => $id_afiliado
                    ];
                    
                    if($hca_cont->fn_crear_camposHc($conn,$h_clinica) == false) {
                        echo '<div class="alert alert-danger" role="alert"><strong>Error creando campos de la Historia Clínica No. {$cons_hc}</strong></div>';
                        exit;    
                    } 
                    // else {
                    //     echo '<p>Se crearon los campos de la Historia clínica</p>';
                    // }

                }else{
                    echo '<div class="alert alert-danger" role="alert"><strong>Error de creación de Historia Clínica No. ' . $cons_hc . '</strong></div>';
                    exit;
                }
                
                // Se neceaitan:  el id de médico, el no de admision, 
                // Calculo de la Edad de un paciente
                // Variables de Entorno (IDAREA, IDMEDICO, PROCEDENCIA, IDSEDE)

            } else {
                echo '<div class="alert alert-danger" role="alert"><strong>Error: Consecutivo no generado!</strong></div>';
                exit;        
            }
            
            // 3 - Se valida la creación de la HC
            // 4 - Se continúa normalmente

        } else {
            if (!isset($_GET['plahcid'])){
                echo '<div class="alert alert-danger" role="alert"><strong>El parámetro Id. Plantilla es requerido</strong></div>';
                exit;
            } else {
                $cons_hc   = $_GET['hcpid'];    // <-- Se reemplaza por variable (la que llega o la que se genera)
                $id_plt    = $_GET['plahcid'];
            }
        }

        /* EDICION DE HC */
        /* Connect using Windows Authentication. */

        $afi       = new Afiliado();
        $autofocus = '';
        $plantilla = $afi->get_Plantilla($cons_hc);  // <-- Obtiene el nombre de la Plantilla

        // Encabezado de página.  Incluye información de Afiliado y Plantilla de HC
        echo '<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active"><a class="nav-link" href="#">Afiliado: 
                    ' . $afi->get_NombreHCA($cons_hc) . '</a>
                    </li>
                    <li class="nav-item active"><a class="nav-link" href="#">Consecutivo:
                    ' . $cons_hc . '</a>
                    </li>
                    <li class="nav-item active"><a class="nav-link" href="#">Plantilla:
                    ' . $plantilla . '</a>
                    </li>
                </ul>
            </div>
        </nav>
        <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><strong>Afiliado:</strong> ' . $afi->get_NombreHCA($cons_hc) . '</li>
            <li class="breadcrumb-item"><strong>Consecutivo:</strong> ' . $cons_hc . '</li>
            <li class="breadcrumb-item active" aria-current="page"><strong>Plantilla: </strong>' . $plantilla . '</li>
        </ol>
        </nav>';

        // Consulta para tarer campos de la historia:
        
        $consulta = "SELECT x.hcpid, x.plahcdid AS campohc, y.plahcid, x.hcpdid, x.secuencia, p.tipocampo, x.alfanumerico, x.fecha, x.memo, x.numerico, x.obs, p.desccampo,
        p.sexo, p.requerido, p.arrastra, p.valordefecto, p.imprimir, p.plahcdid, p.valorminimo, p.valormaximo, p.mayusculas, p.capitalize, p.sololectura,
        p.columna, p.ancho, p.alto, p.formato, p.imprimesindato
        FROM hcpd x JOIN hcp y ON x.hcpid=y.hcpid JOIN plahcd p ON x.plahcdid = p.plahcdid
        WHERE x.hcpid=$cons_hc ORDER BY x.hcpdid, x.secuencia";

        // $consulta = "SELECT CONSECUTIVO,HCAD.CLASEPLANTILLA,HCAD.CAMPO,HCAD.SECUENCIA,NADA,NORMAL,ANORMAL,DETALLAR,NOTAS,TIPOCAMPO,
        // ALFANUMERICO,CONVERT(VARCHAR(10),FECHA,126) AS FECHA,MEMO,OBS,LISTA,ENEPICRISIS,HCADID,MPLD.DESCCAMPO,MPLD.SEXO,MPLD.EDADINI,MPLD.EDADFIN,MPLD.REQUERIDO,
        // MPLD.ARRASTRAINFO,MPLD.TIPOEDICION,MPLD.TIPOARRASTRE,MPLD.DEFAULT1,MPLD.IMPRIMIR 
        // FROM HCAD WITH(INDEX(HCADCONSECUTIVO))  
        // INNER JOIN MPLD WITH(INDEX(MPLDCLASEPLANSECU)) ON MPLD.CLASEPLANTILLA = HCAD.CLASEPLANTILLA AND MPLD.SECUENCIA = HCAD.SECUENCIA 
        // WHERE CONSECUTIVO = '" . $cons_hc . "' ORDER BY HCAD.CONSECUTIVO,HCAD.SECUENCIA";

        $sth = $conn->prepare($consulta);
        $sth->execute();
        $result = $sth->fetchall(PDO::FETCH_ASSOC);

        // Consulta para traer los valores de lista:

        // $sqlLista = "SELECT CAMPO,VALORLISTA FROM MPLDL WHERE CLASEPLANTILLA='" . $afi->get_ClasePlantilla($cons_hc) . "' ";
        $sqlLista = "SELECT x.hcpdlid, x.plahcdid, x.valorlista, x.checkm, x.hcpdid FROM hcpdl x WHERE x.hcpid = $cons_hc";

        $sthLista    = $conn->prepare($sqlLista);
        $sthLista->execute();
        $resultLista = $sthLista->fetchall(PDO::FETCH_ASSOC);
        
        // Creación de la Interfaz Gráfica de la Historia Clínica:

        echo '<div class="container border border-primary rounded">';
        echo '<br><div align="center" class="p-2 bg-info text-white"><h4>' . $plantilla . '</h4></div><br>';
        echo '<div class="row">';
        echo '<div class="col-md-10 mx-auto">';
            echo '<form role="form" id="hca_form">';
            $campos = $sth->rowCount();
            $i = 0;
            $rows = 0;

            $tipocampo = "";

            foreach ($result as $row) {

                $tipocampo      = $row['tipocampo'];         // <-- Tipo del campo de Plantilla
                $campo          = $row['hcpdid'];            // <-- identificador del campo de Plantilla
                $desccampo      = $row['desccampo'];         // <-- Descripción del campo '<strong>' . $row['desccampo'] . '</strong>';
                $secuencia      = $row['secuencia'];         // <-- Secuencia del campo (consecutivo dentro de la Plantilla)
                $alfanumerico   = $row['alfanumerico'];      // <-- Valor en columna alfanumérico   
                $fecha          = $row['fecha'];             // <-- Valor en columna Fecha
                $memo           = $row['memo'];              // <-- Valor en columna Memo
                $numerico       = $row['numerico'];          // <-- Valor en columna Numérico
                $claseplantilla = $row['plahcid'];           // <-- Identificador de la Plantilla de H. Clínica
                $campoHc        = $row['campohc'];           // <-- Identificador del campo en la Palntilla de H. Clínica   
                $a              = 'autofocus';
                $idvalorlista   = 0;

                // $i se refiere al número de columnas que se van a usar (se trabaja junto con la constante MAX_COLS)

                if (rtrim(ltrim($tipocampo)) != 'TituloN1' && rtrim(ltrim($tipocampo)) != 'TituloN2' && rtrim(ltrim($tipocampo)) != 'TituloN3') {
                    if ($i == 0) {
                        echo '<div class="form-group row">';    // <-- Si no es un tipo de título y num columnas es 0 abre un div
                    }
                }else{
                    if ($i != 0) {
                        echo '</div>';      // <-- Si es un tipo de título y num columnas es mayor que cero cierro el div (los tipos título abren un Div)
                        $i=0;
                    }
                }

                switch (rtrim(ltrim($tipocampo))) {
                    case "TituloN1":
                        {
                            echo '<div class="p-2 bg-info text-white" name="STC_'.$campo.'_'.$secuencia.'" id=STC_'.$campo.'_'.$secuencia.'">'.$desccampo.'</div>';
                            break;
                        }
                    case "TituloN2":
                        {
                            echo '<div class="p-2 bg-warning text-white" name="STC_'.$campo.'_'.$secuencia.'" id=STC_'.$campo.'_'.$secuencia.'">'.$desccampo.'</div>';
                            break;
                        }
                    case "TituloN3":
                        {
                            echo '<div class="p-2 bg-success text-white" name="STC_'.$campo.'_'.$secuencia.'" id=STC_'.$campo.'_'.$secuencia.'">'.$desccampo.'</div>';
                            break;
                        }    
                    case "Textocorto":
                        {    
                            echo '<div class="col">';
                                echo '<label>'.$desccampo.'</label>';
                                echo '<input type="text" name="EDT_'.$campo.'_'.$secuencia.'" id="EDT_'.$campo.'_'.$secuencia.'" value="'.$alfanumerico.'" size="50" class="form-control" onblur="save_valor(this.id,\''.$tipocampo.'\',\''.$cons_hc.'\')" ';
                                echo ' onfocus="ingreso_campo(this.id)">';
                            echo '</div>'; // '.$a.' 
                            break;
                        }
                    case "Numerico":
                        {    
                            echo '<div class="col">';
                                echo '<label>'.$desccampo.'</label>';
                                echo '<input type="number" name="EDT_'.$campo.'_'.$secuencia.'" id="EDT_'.$campo.'_'.$secuencia.'" value="'.$numerico.'" size="50" class="form-control" onblur="save_valor(this.id,\''.$tipocampo.'\',\''.$cons_hc.'\')" ';
                                echo ' onfocus="ingreso_campo(this.id)">';
                            echo '</div>'; // '.$a.' 
                            break;
                        }    
                    case "Textolargo":
                        {
                            echo '<div class="col">';
                                echo'<label>'.$desccampo.'</label>';
                                echo '<textarea name="TEXT_'.$campo.'_'.$secuencia.'" id="TEXT_'.$campo.'_'.$secuencia.'" cols="60" rows="4" class="form-control" '; // >'.$memo.'</textarea>';
                                echo 'onfocus="ingreso_campo(this.id)" ';
                                echo 'onblur="save_valor(this.id,\''.$tipocampo.'\',\''.$cons_hc.'\')" >';
                                echo ''.$memo.'</textarea>'; 
                            echo '</div>'; //'.$a.' 
                            break;
                        } 
                    case "Fecha":
                        {
                            // value= ?PHP  echo date('Y-m-d',strtotime($fecha ));  ?
                            echo '<div class="col">';
                                echo '<label>'.$desccampo.'</label>';
                                echo '<input type="date" name="EDT_'.$campo.'_'.$secuencia.'" id="EDT_'.$campo.'_'.$secuencia.'" value="'.date('Y-m-d',strtotime($fecha)).'" class="form-control" ';
                                echo 'onfocus="ingreso_campo(this.id)" ';
                                echo 'onblur="save_valor(this.id,\''.$tipocampo.'\',\''.$cons_hc.'\')" >'; //.$fecha.
                                // echo 'Fecha: ' . date('Y-m-d',strtotime($fecha));
                            echo '</div>'; // '.$a.' 
                            break;
                        }
                    case "Hora":
                        {
                            echo '<div class="col">';
                                echo '<label>'.$desccampo.'</label>';
                                echo '<input type="time" name="EDT_'.$campo.'_'.$secuencia.'" id="EDT_'.$campo.'_'.$secuencia.'" value="'.date('H:i',strtotime($fecha)).'" class="form-control" ';
                                echo 'onfocus="ingreso_campo(this.id)" ';
                                echo 'onblur="save_valor(this.id,\''.$tipocampo.'\',\''.$cons_hc.'\')" >';
                            echo '</div>'; // '.$a.' 
                            break;
                        }
                    case "Fechahora":
                        {
                            echo '<div class="col">';
                                echo '<label>'.$desccampo.'</label>';
                                // echo '<input type="datetime-local" '.$a.' name="EDT_'.$campo.'_'.$secuencia.'" id="EDT_'.$campo.'_'.$secuencia.'" value="'.$fecha.'" class="form-control" ';
                                echo '<input type="datetime-local" name="EDT_'.$campo.'_'.$secuencia.'" id="EDT_'.$campo.'_'.$secuencia.'" value="'.date('Y-m-d',strtotime($fecha)).'T'.date('H:i',strtotime($fecha)).'" class="form-control" ';
                                // date_format($date, 'Y-m-d H:i:s');
                                echo 'onfocus="ingreso_campo(this.id)" ';
                                echo 'onblur="save_valor(this.id,\''.$tipocampo.'\',\''.$cons_hc.'\')" >';
                                // echo 'Fecha: ' . date('Y-m-d H:i:s',strtotime($fecha));
                            echo '</div>'; // '.$a.' 
                            break;
                        }        
                    case "Lista":
                        {
                            echo '<div class="col">';
                                echo '<label>'.$desccampo.'</label>';
                                echo '<select name="COMBO_'.$campo.'_'.$secuencia.'" class="form-control" id="COMBO_'.$campo.'_'.$secuencia.'" '; //
                                echo 'onChange="save_valor(this.id,\''.$tipocampo.'\',\''.$cons_hc.'\')" >';                                  
                                
                                    echo '<option value=""></option>';                          // <-- Primera opción de la lista es vacío (null)
                                    foreach ($resultLista as $rowx) {
                                        if ($rowx['plahcdid'] == $campoHc) {
                                            if ($rowx['valorlista'] == $alfanumerico) {
                                                $sel = 'selected="selected"';
                                            } else {
                                                $sel = '';
                                            }    
                                            // Averiguar con Fredy si se puede agregar el campo VALOR_GUARDAR a los campos de tipo [Lista]
                                            echo '<option value="'.$rowx['valorlista'].'" '.$sel.' >'.$rowx['valorlista'].'</option>';
                                        }
                                    }

                                echo '</select>';
                            echo '</div>'; // '.$a.' 
                            break;
                        }
                    case "Checkbox":
                        {
                            // SELECT x.hcpdlid, x.plahcdid, x.valorlista, x.checkm, x.hcpdid FROM hcpdl x WHERE x.hcpid = $cons_hc
                            echo '<div class="col">';
                                foreach ($resultLista as $rowx) {
                                    if ($rowx['plahcdid'] == $campoHc) {
                                        $idvalorlista = $rowx['hcpdlid'];
                                        echo '<input type="checkbox" name="CBOX_'.$campo.'_'.$secuencia.'_'.$idvalorlista.'" id="CBOX_'.$campo.'_'.$secuencia.'_'.$idvalorlista.'" value="CBOX_'.$campo.'_'.$secuencia.'_'.$idvalorlista.'" ';
                                        echo 'onChange="save_valor(this.id,\''.$tipocampo.'\',\''.$cons_hc.'\')" >';
                                        echo '<label for="CBOX_'.$campo.'_'.$secuencia.'" style="display: inline;"> '.$rowx['valorlista'].'</label>'; //
                                        echo '<br>';
                                    }
                                }
                            echo '</div>';
                            break;            
                        }    
                }

                $rows++;

                if (rtrim(ltrim($tipocampo)) != 'TituloN1' && rtrim(ltrim($tipocampo)) != 'TituloN2' && rtrim(ltrim($tipocampo)) != 'TituloN3') {
                    $i++;
                }
                
                if ($i == MAX_COLS) {                       // <-- Si ya van dos columnas cortar el form group row
                    echo '</div>';
                    $i = 0;
                }

                if ($rows == $campos) {
                    echo '</div>';
                }

                //echo '<hr class="style4">';  // <-- El foco queda automáticamente en el primer campo
                if ($autofocus == '') {
                    $autofocus = 'Y';
                    $a = '';
                }

            }

            echo '<div class="row">';
                echo '<div class="col">&nbsp;</div>';
            echo '</div>';

            echo '<div class="row">';
                echo '<div class="col">';
                    echo '<button type="button" class="btn btn-primary" name="guardahc"  id="guardahc" >Guardar y Salir</button>';
                    echo '&nbsp;&nbsp;';
                    echo '<button type="button" class="btn btn-danger"  name="cancelahc" id="cancelahc">Cancelar</button>';
                echo '</div>';
            echo '</div>';
            
            echo '<br><br>';

            echo '</form>';
        echo '</div>';
        echo '</div>';

        // echo '<script src="js/jquery-3.3.1.min.js" async="async"></script>';
        // echo '<script src="js/popper.min.js" async="async"></script>';
        // echo '<script src="js/bootstrap.min.js" async="async"></script>';
        // echo '<script src="js/view_controler/view_cont_hca/vc_hca.js" async="async"></script>';

    ?>
    <!-- <script src="js/jquery-3.3.1.slim.min.js"></script> -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/view_controler/view_cont_hca/vc_hca.js"></script>
</body>
</html>