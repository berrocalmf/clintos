<?php
/**
 * Created by PhpStorm.
 * User: acost
 * Date: 10/09/2018
 * Time: 14:45
 */

class Historia
{
    private $consecutivo;
    private $conn;

    public function __construct()
    {
        $this->conn=new Database();
    }

    function get_NombreHCA($idafiliado){

        $consulta = "SELECT COALESCE(PNOMBRE,'')+' '+COALESCE(SNOMBRE,'')+' '+COALESCE(PAPELLIDO,'')+' '+
COALESCE(SAPELLIDO,'') AS NOMBRE FROM HCA INNER JOIN AFI ON HCA.IDAFILIADO=AFI.IDAFILIADO 
 WHERE HCA.CONSECUTIVO='".$idafiliado."'";
        $sth = $this->conn->prepare($consulta);
        $sth->execute();
        $result = $sth->fetch(PDO::FETCH_ASSOC);
        return $result['NOMBRE'];
    }

    function get_Plantilla($consecutivo){

        $consulta = "SELECT DESCPLANTILLA FROM HCA INNER JOIN MPL ON HCA.CLASEPLANTILLA=MPL.CLASEPLANTILLA 
 WHERE HCA.CONSECUTIVO='".$consecutivo."'";
        $sth = $this->conn->prepare($consulta);
        $sth->execute();
        $result = $sth->fetch(PDO::FETCH_ASSOC);
        return $result['DESCPLANTILLA'];
    }
}