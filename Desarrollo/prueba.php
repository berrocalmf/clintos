<?php
// -----------------------------------------------------------
// DATOS DEL SERVIDOR DE WEB
// -----------------------------------------------------------

// $indicesServer = array('PHP_SELF',
// 'argv',
// 'argc',
// 'GATEWAY_INTERFACE',
// 'SERVER_ADDR',
// 'SERVER_NAME',
// 'SERVER_SOFTWARE',
// 'SERVER_PROTOCOL',
// 'REQUEST_METHOD',
// 'REQUEST_TIME',
// 'REQUEST_TIME_FLOAT',
// 'QUERY_STRING',
// 'DOCUMENT_ROOT',
// 'HTTP_ACCEPT',
// 'HTTP_ACCEPT_CHARSET',
// 'HTTP_ACCEPT_ENCODING',
// 'HTTP_ACCEPT_LANGUAGE',
// 'HTTP_CONNECTION',
// 'HTTP_HOST',
// 'HTTP_REFERER',
// 'HTTP_USER_AGENT',
// 'HTTPS',
// 'REMOTE_ADDR',
// 'REMOTE_HOST',
// 'REMOTE_PORT',
// 'REMOTE_USER',
// 'REDIRECT_REMOTE_USER',
// 'SCRIPT_FILENAME',
// 'SERVER_ADMIN',
// 'SERVER_PORT',
// 'SERVER_SIGNATURE',
// 'PATH_TRANSLATED',
// 'SCRIPT_NAME',
// 'REQUEST_URI',
// 'PHP_AUTH_DIGEST',
// 'PHP_AUTH_USER',
// 'PHP_AUTH_PW',
// 'AUTH_TYPE',
// 'PATH_INFO',
// 'ORIG_PATH_INFO') ;

// echo '<table cellpadding="10">' ;
// foreach ($indicesServer as $arg) {
//     if (isset($_SERVER[$arg])) {
//         echo '<tr><td>'.$arg.'</td><td>' . $_SERVER[$arg] . '</td></tr>' ;
//     }
//     else {
//         echo '<tr><td>'.$arg.'</td><td>-</td></tr>' ;
//     }
// }
// echo '</table>' ;



// -----------------------------------------------------------
// GENERACION DE CONSECUTIVO
// -----------------------------------------------------------

        include_once 'controller/hca_cont/hca_controler.php';

        include_once 'Afiliado.php';
        include_once 'funciones.php';

        $conn = new Database();

        $cons_hc = '01' . str_pad(fn_genConsecutivo(1, 1, "@HC", "0", $conn),8,"0",STR_PAD_LEFT);
        $nombre_host = gethostbyaddr($_SERVER['REMOTE_ADDR']);

        echo "Consecutivo: " . $cons_hc . " Host: " . $nombre_host;

        // $hca_cont = new hca_controler();

        // $h_clinica = [
        //     "CONSECUTIVO"        => $cons_hc,
        //     "NOADMISION"         => "0100000001",
        //     "IDAFILIADO"         => "1001549572",
        //     "IDMEDICO"           => "1067874684",
        //     "AFITTODID"          => 0,
        //     "CLASEPLANTILLA"     => "HCURG",
        //     "IDMEDICODILIGENCIA" => "12345",
        //     "EDAD"               => "19 anios",
        //     "IDAREA"             => "4105",
        //     "SYS_ComputerName"   => $nombre_host,
        //     "PROCEDENCIA"        => "QX",
        //     "GLASGOW"            => "Alerta",
        //     "CLASE"              => "HC",
        //     "ESTADO"             => "Activa",
        //     "IDSEDE"             => "07"    
        // ];

        // if($hca_cont->fn_crear_registro_hc($conn,$h_clinica) == true) {
        //     echo "Creada HC No. " . $cons_hc;

        //     $dataHc = [
        //         "CONSECUTIVO"    => $cons_hc,
        //         "CLASEPLANTILLA" => "HCURG",
        //         "IDAFILIADO"     => "1001549572"
        //     ];

        //     if($hca_cont->fn_crear_camposHc($conn,$h_clinica) == false) {
        //         echo '<div class="alert alert-danger" role="alert"><strong>Error creando campos de la Historia Clínica No. {$cons_hc}</strong></div>';
        //         exit;    
        //     }

        // }else{
        //     echo "Error de creación de Historia Clínica No. " . $cons_hc;
        // }


// -----------------------------------------------------------
// ARRAYS ASOCIATIVOS
// -----------------------------------------------------------

    // $h_clinica = [
    //     "CONSECUTIVO" => "010101",
    //     "NOADMISION"  => "0100000001",
    //     "IDAFILIADO"  => "",
    //     "IDMEDICO"    => "",
    //     "AFITTODID"   => 0
    // ];

    // foreach($h_clinica as $campo=>$valor) {
    //     echo "El campo " . $campo . " tiene el valor " . $valor;
    // 	echo "<br>";
    // }

    // print_r($h_clinica);

// -----------------------------------------------------------
// CALCULO DE FECHAS ==============================================================================
// -----------------------------------------------------------

// include_once 'funciones.php';
// $fechanac = '2018-05-12 00:00:00.000';
// $fechaact = date("Y-m-d");

// echo 'Edad: ' . fn_edadPersona($fechanac, $fechaact);
// echo '<br>';
// echo 'Edad: ' . fn_edadPersona($fechanac, $fechaact, 2, 'y');
// echo '<br>';
// echo 'Edad: ' . fn_edadPersona($fechanac, $fechaact, 2, 'm');
// echo '<br>';
// echo 'Edad: ' . fn_edadPersona($fechanac, $fechaact, 2, 'd');
// echo '<br>';
// echo 'Edad: ' . fn_edadPersona($fechanac, $fechaact, 2, 'cy');
// echo '<br>';
// echo 'Edad: ' . fn_edadPersona($fechanac, $fechaact, 2, 'cm');
// echo '<br>';
// echo 'Edad: ' . fn_edadPersona($fechanac, $fechaact, 2, 'cd');

// $nombre_host = gethostbyaddr($_SERVER['REMOTE_ADDR']);
// echo 'Nombre del equipo: ' . $nombre_host;
// =================================================================================================


// echo '<script>';
// echo 'console.log("LLego hasta aqui...")';
// echo '</script>';

// -----------------------------------------------------------
// Prueba de conexion con Klinike
// -----------------------------------------------------------

        // include_once 'controller/hca_cont/hca_controler.php';
        // include_once 'Afiliado.php';
        // include_once 'funciones.php';

        // $pac = new Afiliado();
        // print_r($pac->get_Edad(33));

?>