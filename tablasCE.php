<?php
    include_once("database.class.php");

    /**
     * Este archivo hace parte de las vistas del sistema de información
     */

	if(isset($_POST["accion"]) && isset($_POST["idAfiliado"])) {
        $command = $_POST["accion"];
        $idAfi   = $_POST["idAfiliado"];
        $varHtml = "";
        $conn    = new Database();

		switch($command) {
            case '1':

                // Listado de Historias clínicas de un paciente:

                $consulta = "SELECT x.CONSECUTIVO as idHistoria, x.HCAID as hcaid, x.FECHA as fecha, x.IDMEDICO as idmedico, ";
                $consulta .= "x.CLASEPLANTILLA as idPlantilla, x.IDAFILIADO as pacid FROM HCA x WHERE x.IDAFILIADO = :idafi";
                $consulta .= " ORDER BY x.FECHA DESC";

                $sth    = $conn->prepare($consulta);
                $params = array('idafi'=>$idAfi);
                $sth->execute($params);
                $result = $sth->fetchall(PDO::FETCH_ASSOC);

                if (sizeof($result) > 0) {

                    $varHtml="";
                    $varHtml="<h6>Historia Cl&iacute;nica</h6>";
                    $varHtml.="<table class='table table-striped' data-location='left' data-makeup='topbar' ruleindex='24 228'>";
                        $varHtml.="<thead><tr><th colspan='2'>Acciones</th><th>Consecutivo</th><th>Id. Historia</th><th>Fecha</th><th>Id M&eacute;dico</th><th>Plantilla</th></tr></thead>";

                        $varHtml.="<tbody>";
                            foreach ($result as $row) {
                                $varHtml.= "<tr><td><a href='win_hcaphp.php?action=edit&hcpid=" . $row['idHistoria'] . "&pacid=" . $row['pacid'] . "' target='_blank' title='Modificar'><i class='fa fa-pencil fa-fw'></i></a></td>
                                <td><a href='win_hcaphp.php?action=view&hcpid=" . $row['idHistoria'] . "&pacid=" . $row['pacid'] . "' target='_blank' title='Visualizar'><i class='fa fa-eye'></i></a></td>
                                <td>" . $row['idHistoria'] . "</td><td>" . $row['hcaid'] . "</td>
                                <td>" . $row['fecha'] . "</td><td>" . $row['idmedico'] . "</td><td>" . $row['idPlantilla'] . "</td></tr>";
                            }
                        $varHtml.="</tbody>";

                    $varHtml.="</table>";
                    echo $varHtml;

                } else {

                    $varHtml="";
                    $varHtml="<h6>Historia Cl&iacute;nica</h6>";
                    $varHtml.="<table class='table table-striped' data-location='left' data-makeup='topbar' ruleindex='24 228'>";
                    $varHtml.="<thead><tr><th colspan='2'></th><th>Consecutivo</th><th>Clase Plantilla</th><th>Fecha</th><th>Id M&eacute;dico</th><th>DX</th></tr></thead>";
                    $varHtml.="<tbody>";
                    $varHtml.="</tbody>";
                    $varHtml.="</table>";
                    $varHtml.="<b>Sin Historia Cl&iacute;nica</b>";
                    echo $varHtml;

                }

                break;
            
            case '2':

                // CONSULTAR CON FREDY DIAZ SOBRE LAS ORDENES MEDICAS O AUTORIZACIONES (Sólo manejaremos ome?)
                // Listar de la tabla ome (Órdenes médicas)

                $consulta  = "SELECT OMEID as omeid, FECHA as fechaome, CONSECUTIVO as consecutivo, IDMEDICO as medid, OMEMID as tomid";
                $consulta .= " FROM OME WHERE IDAFILIADO = :idafi";
                $consulta .= " ORDER BY fechaome DESC";

                $sth      = $conn->prepare($consulta);
                $params   = array('idafi'=>$idAfi);
                $sth->execute($params);
                $result   = $sth->fetchall(PDO::FETCH_ASSOC);

                if (sizeof($result) > 0) {
                    $varHtml="";
                    $varHtml="<h6>Ordenes M&eacute;dicas</h6>";
                    $varHtml.="<table class='table table-striped' data-location='left' data-makeup='topbar' ruleindex='24 228'>";
                        $varHtml.="<thead><tr><th colspan='2'></th><th>Id.</th><th>Fecha</th><th>Consecutivo</th><th>Solicitante</th><th>Tipo Orden</th></tr></thead>";

                        $varHtml.="<tbody>";
                            foreach ($result as $row) {
                                $varHtml.="<tr><td><a href='#' target='_blank' title='Modificar'><i class='fa fa-pencil fa-fw'></i></a></td>
                                <td><a href='#' target='_blank' title='Órdenes Médicas'><i class='fa fa-book fa-fw'></i></a></td>
                                <td>" . $row['omeid'] . "</td><td>" . $row['fechaome'] . "</td>
                                <td>" . $row['consecutivo'] . "</td><td>" . $row['medid'] . "</td><td>" . $row['tomid'] . "</td></tr>";
                            }
                        $varHtml.="</tbody>";

                    $varHtml.="</table>";
                    echo $varHtml;
                } else {
                    $varHtml="";
                    $varHtml="<h6>Ordenes M&eacute;dicas</h6>";
                    $varHtml.="<table class='table table-striped' data-location='left' data-makeup='topbar' ruleindex='24 228'>";
                        $varHtml.="<thead><tr><th colspan='2'></th><th>Id.</th><th>Fecha</th><th>Consecutivo</th><th>Solicitante</th><th>Tipo Orden</th></tr></thead>";
                        $varHtml.="<tbody>";
                        $varHtml.="</tbody>";
                    $varHtml.="</table>";
                    $varHtml.="<b>Sin Ordenes M&eacute;dicas</b>";
                    echo $varHtml;
                }

                break;

            case '3':

                // Listado de citas del paciente:

                $consulta  = "SELECT x.CONSECUTIVO as consecutivo, x.FECHA as fecha, x.FECHAATENCION as fechaAtencion, x.IDMEDICO as idMedico,";
                $consulta .= "x.IDDX as iddx FROM cit x WHERE x.IDAFILIADO=:idafi";
                $consulta .= " ORDER BY x.FECHA DESC";

                $sth       = $conn->prepare($consulta);
                $sth->execute(array('idafi'=>$idAfi));
                $result    = $sth->fetchall(PDO::FETCH_ASSOC);

                if (sizeof($result) > 0) {
                    
                    $varHtml="";
                    $varHtml  = "<h6>Citas</h6>";
                    $varHtml .= "<table class='table table-striped' data-location='left' data-makeup='topbar' ruleindex='24 228'>";
                        $varHtml .= "<thead><tr><th colspan='2'></th><th>Consecutivo</th><th>Fecha</th><th>Fecha Atenc.</th><th>Id M&eacute;dico</th><th>DX</th></tr></thead>";
                        
                        $varHtml .= "<tbody>";
                            foreach ($result as $row) {
                                $varHtml .= "<tr><td><a href='#' target='_blank' title='Modificar'><i class='fa fa-pencil fa-fw'></a></td>
                                <td><a href='#' target='_blank' title='Órdenes Médicas'><i class='fa fa-book fa-fw'></a></td>
                                <td>" . $row['consecutivo'] . "</td><td>" . $row['fecha'] . "</td>
                                <td>" . $row['fechaAtencion'] . "</td><td>" . $row['idMedico'] . "</td><td>" . $row['iddx'] . "</td></tr>";
                            }
                        $varHtml.="</tbody>";

                    $varHtml.="</table>";
                    echo $varHtml;   
                } else {
                    $varHtml="";
                    $varHtml="<h6>Citas</h6>";
                    $varHtml.="<table class='table table-striped' data-location='left' data-makeup='topbar' ruleindex='24 228'>";
                        $varHtml.="<thead><tr><th colspan='2'>Opciones</th><th>Consecutivo</th><th>Fecha</th><th>Fecha Atenc.</th><th>Id M&eacute;dico</th><th>DX</th></tr></thead>";
                        $varHtml.="<tbody>";
                        $varHtml.="</tbody>";
                    $varHtml.="</table>";
                    $varHtml.="<b>Sin Citas en el Sistema</b>";
                    echo $varHtml;
                }

                break;

			default:
                $varHtml.="<td>Sin estado</td>";
		}

	} else {
		$varHtml.="<td>Par&aacute;metros inv&aacute;lidos o sin Valor</td>";
		echo $varHtml;
	}

?>
