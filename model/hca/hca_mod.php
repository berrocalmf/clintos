<?php
    /**
     * hca table model
     * Dev. FMBM
     * Refactored: 21.01.2021 fberrocalm
     */
    class hca_mod {

        private $cacheOk;
        private $cacheData;

        public function __construct() {
            $this->resetCache();
        }

        public function resetCache() {
            $this->cacheOk   = false;
            $this->cacheData = null;
        }

        public function createValueObject() {
            return new hca_mod();
        }

        public function getObject(&$conn, $consecutivo) {
            // $valueObject = $this->createValueObject();
            // $valueObject->setMarid($marid);
            // $this->load($conn, $valueObject);
            // return $valueObject;
        }

        /**
         * Actualiza el valor de un campo de Historia clínica
         * @hcpdid : id del campo de la HC a Acltualizar (number)
         * @valor  : Nuevo valor que tendrá el campo segun su tipo
         * @hcpid  : Id de la Historia clpinica
         * Ing. FMBM - 24-Ago-2019
        */

        // -------------------------------------------------------------------------------------
        // Cambiamos $nom_campo por $hcpdid
        // El parámetro $tipo_campo de deja hasta probar el funcionamiento y su necesidad
        // El campo $consecutivo se cambia por $hcpid
        // -------------------------------------------------------------------------------------
        // Llamado: $hca_c->salvarCampo($c, $hcpdid, $tipo_campo, $valor_campo, $hcpid);
        public function salvarCampo(&$conn, $hcpdid, $tipo_campo, $valor, $hcpid) {

            // $hcpdid => hcadid            $hcpid => hcaid 

            $qry   = "";

            if( !(null == $hcpdid) && !("" == $tipo_campo) && !($valor == "") && !(null == $hcpid)) {
                
                $arr_campo = explode("_", $hcpdid);
                $campo     = $arr_campo[1];
                $sec       = $arr_campo[2]; 
                $idCampo   = $hcpdid;

                switch(ltrim(rtrim($tipo_campo))) {

                    // 1. Armar el query para la actualización:

                    case "Titulo":
                        break;
                    case "Subtitulo": 
                        break;
                    case "Alfanumerico":
                        $qry = "UPDATE hcad SET ALFANUMERICO='{$valor}' WHERE hcadid={$campo}"; 
                        // $qry = "UPDATE hcpd SET alfanumerico='{$valor}' WHERE hcpdid={$campo}"; 
                        break;
                    case "Numerico":
                        $qry = "UPDATE hcad SET NUMERICO='{$valor}' WHERE hcadid={$campo}"; 
                        // $qry = "UPDATE hcpd SET numerico='{$valor}' WHERE hcpdid={$campo}"; 
                        break; 
                    case "Memo":
                        $qry = "UPDATE hcad SET MEMO='{$valor}' WHERE hcadid={$campo}"; 
                        // $qry = "UPDATE hcpd SET memo='{$valor}' WHERE hcpdid={$campo}"; 
                        break;
                    case "Fecha":
                        $qry = "UPDATE hcad SET FECHA='{$valor}' WHERE hcadid={$campo}"; 
                        // $qry = "UPDATE hcpd SET fecha='{$valor}' WHERE hcpdid={$campo}"; 
                        break;
                    // case "Hora":
                    //     $qry = "UPDATE hcpd SET fecha='{$valor}' WHERE hcpdid={$campo}"; 
                    //     break;
                    // case "Fechahora":
                    //     $qry = "UPDATE hcpd SET fecha='{$valor}' WHERE hcpdid={$campo}"; 
                    //     break;
                    case "Lista":
                        $qry = "UPDATE hcad SET ALFANUMERICO='{$valor}' WHERE hcadid={$campo}"; 
                        // $qry = "UPDATE hcpd SET alfanumerico='{$valor}' WHERE hcpdid={$campo}"; 
                        break;
                    case "Checkbox":
                        $idCampo = $arr_campo[3]; 
                        $qry = "UPDATE hcadl SET CHECKM={$valor} WHERE hcadlid={$idCampo}";
                        // $qry = "UPDATE hcpdl SET checkm={$valor} WHERE hcpdlid={$idCampo}";
                        break;    
                }

                $res = $this->databaseUpdate($conn, $qry);

                if( $res != 1 ) {
                    return 0;
                }

                return 1;

            }

        }

        # Crear el encabezado de HC nueva:

        // $conn    : Conexión activa a la Base de Datos
        // $data_hc : Array con los datos para crear el registro de historia clínica
        // Dev. FMBM 03/02/2020

        public function create_record_hc(&$conn, $data_hc) {
            
            // $fechaact = date_create(now());
            $id_hca = 0;
            $ateid  = "";
            $exito;

            if ($data_hc["ateid"]=="" || $data_hc["ateid"]==null) {
                $ateid = "null";
            } else {
                $ateid = $data_hc["ateid"];
            }
             
            $qry = "";
            $qry = "INSERT INTO hca (CONSECUTIVO,NOADMISION,IDAFILIADO,IDMEDICO,AFITTODID,CLASEPLANTILLA,IDMEDICODILIGENCIA,EDAD,IDAREA,";
            $qry .= "SYS_ComputerName,PROCEDENCIA,GLASGOW,CLASE,ESTADO,IDSEDE,FECHA,ALY_ATEID,IMPRESO,N_HISTORIA,REVISAESP,PIDEDX,PIDESV,";
            $qry .= "LAPSOATENCION,INVESTIGACION,ALTA,ITEMHADM,AFITTOID,TEMP,TALLA,PESO) VALUES (";
            $qry .= "'" . $data_hc["consecutivo"] . "','" . $data_hc["noadmision"] . "','" . $data_hc["pacid"] . "',";
            $qry .= "'" . $data_hc["idmedico"] . "'," . $data_hc["afittodid"] . ",'" . $data_hc["plahcid"] . "','" . $data_hc["IDMEDICODILIGENCIA"] . "',";
            $qry .= "'" . $data_hc["edad"] . "','" . $data_hc["IDAREA"] . "','" . $data_hc["SYS_ComputerName"] . "','" . $data_hc["PROCEDENCIA"] . "',";
            $qry .= "'" . $data_hc["GLASGOW"] . "','" . $data_hc["CLASE"] . "','" . $data_hc["ESTADO"] . "','" . $data_hc["IDSEDE"] . "',getdate()," . $data_hc["ateid"];
            $qry .= ",0,'" . $data_hc["nodocumento"] . "',0,0,0,0,0,0,0,0,0.00,0.00,0.00)";

            $res = $this->databaseUpdate($conn, $qry);

            if( $res != 1 ) {
                $exito = false;
                $id_hca = 0;
            } else {
                // $sql  = "SELECT last_insert_id()";
                // $qry1 = $conn->prepare($sql);
                // $qry1->execute();
                // $result = $qry1->fetchAll();
                // $id_hca = 0;
                // foreach($result as $row) {
                //     $id_hca = $row[0];
                // }

                $exito = true;
            }

            // return $id_hca;
            return $exito;
        }

        # Actualiza el diagnóstico de la Historia Clínica

        public function set_hcDx(&$conn,$data_hc) {
            $qry = "";
            $qry = "UPDATE HCA SET IDDX=" . $data_hc["mdxid"] . " WHERE CONSECUTIVO='" . $data_hc["hcpid"] . "'";

            $res = $this->databaseUpdate($conn, $qry);

            if( $res != 1 ) {
                return false;
            } else {
                return true;
            }
        }

        # Crea los campos del registro de historia clínica y los valores de lista y checks (hcpd, hcpdl)

        // $conn    : Conexión activa a la Base de Datos
        // $data_hc : Array con los datos para crear el registro de historia clínica
        // Dev. FMBM 03/02/2020

        public function to_create_hc_fields(&$conn, $data_hc) {
            
            $cnshc     = $data_hc["CONSECUTIVO"];
            $plantilla = $data_hc["CLASEPLANTILLA"];
            $paciente  = $data_hc["IDAFILIADO"];
            
            $sql        = "exec spc_HCAD_Arrastre ?,?,?";
            $query      = $conn->prepare($sql);
            $parametros = array($cnshc, $plantilla, $paciente);

            $res = $query->execute($parametros);

            if( $res != 1 ) {
                return false;
            }

            return true;
        }

        # Retorna el valor del campo LAPSOMODIFICAHC de la tabla MPL
        # Tiempo en minutos que se permite la edición de una Historia Clínica
        # 19.06.2021 - fberrocalm

        public function getTiempoEdicionHC (&$conn, $consecutivohc) {
            try {
                if ($consecutivohc == "" || $consecutivohc == null) {
                    $valorLapso = 0;
                } else {
                    $consulta = "SELECT y.LAPSOMODIFICAHC FROM HCA x JOIN MPL y ON x.CLASEPLANTILLA=y.CLASEPLANTILLA WHERE x.CONSECUTIVO='{$consecutivohc}'";
                    $sth      = $conn->prepare($consulta);
                    $sth->execute();
                    $result   = $sth->fetch(PDO::FETCH_ASSOC);

                    if (is_array($result)) {
                        $valorLapso = $result['LAPSOMODIFICAHC'];
                    } else {
                        $valorLapso = 0;
                    }
                }

                return $valorLapso;

            } catch (PDOException $e) {
                print "!Error¡: " . $e->getMessage();
            } 
        }

        # Retorna el valor del campo solicitado por la App cliente para la historia con consecutivo $consecutivohc
        # 19.06.2021 - fberrocalm

        public function getCampoHistoria (&$conn, $consecutivohc, $campo) {
            try {
                if ($consecutivohc == "" || $consecutivohc == null || $campo == "" || $campo == null) {
                    $valorCampo = "";
                } else {
                    $consulta = "SELECT {$campo} FROM HCA WHERE CONSECUTIVO='{$consecutivohc}'";
                    $sth      = $conn->prepare($consulta);
                    $sth->execute();
                    $result   = $sth->fetch(PDO::FETCH_ASSOC);

                    if (is_array($result)) {
                        $valorCampo = $result[$campo];
                    } else {
                        $valorCampo = 0;
                    }
                }

                return valorCampo;

            } catch (PDOException $e) {
                print "!Error¡: " . $e->getMessage();
            }
        }

        /**
         * Actualiza la Base de Datos
         * @sql: Consulta que actualiza la Base de datos
         * Ing. FMBM - 24-Ago-2019
        */
        public function databaseUpdate(&$conn, &$sql) {
            $query  = $conn->prepare($sql);
            // var_dump($sql);
            $result = $query->execute();
            $this->resetCache();
            return $result;
        }

    }
?>
