<?php
    require_once '../../database.class.php';
    require_once '../../model/hca/hca_mod.php';
    
    // require_once '../../database.class.php';
    // require_once '../../model/hca/hca_mod.php';

    class hca_controler {

        private $c; // <-- Apuntador de tipo conexión de Base de Datos

        public function __construct() {
        }
        
        // Utilizada para crear un nuevo registro de historia clinica:
        public function fn_crear_registro_hc(&$connIn, $data_hc) {
            
            // Se establece la conexión, si no existe se crea:
            if($connIn === null) {
                $this->c = new Database();
            } else {
                $this->c = $connIn;
            }

            try {
                $hca = new hca_mod();
                $res = $hca->create_record_hc($this->c,$data_hc);
            
                return $res;
            } catch ( Exception $e ) {
                $res = false;
                return $res;   
            }

        }

        // función para crear los campos y valores de lista para la H Clínica    
        public function fn_crear_camposHc(&$connIn, $data_hc) {
            // Se establece la conexión, si no existe se crea:
            $this->c = $connIn;

            if($this->c === null) {
                $this->c = new Database();
            } 

            try {
                $hca = new hca_mod();
                $res = $hca->to_create_hc_fields($this->c, $data_hc);
                return $res;
            } catch ( Exception $e ) {
                $res = false;
                return $res;   
            }
        }

        // Función para actualizar el diagnóstico de una H. Clínica

        public function fn_setDx(&$connIn, $data_hc) {

            if($connIn === null) {
                $this->c = new Database();
            } else {
                $this->c = $connIn;
            }

            try {
                $hca = new hca_mod();
                $res = $hca->set_hcDx($this->c, $data_hc);
                return $res;
            } catch ( Exception $e ) {
                $res = false;
                return $res;   
            }                
        }

        # Valida si se puede o no editar una historia clínica
        public function fn_validarEdicion () {
            
        }

    }

?>
