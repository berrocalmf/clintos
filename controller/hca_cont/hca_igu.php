<?php

// SOLO CODIGO SERVIDOR RETORNA UNA PAGINA WEB O UN ERROR SI CORRESPONDE

// Imports:

    require_once '../../Afiliado.php';
    require_once '../../funciones.php';
    require_once 'hca_controler.php';								// include_once './controller/hca_cont/hca_controler.php'


// Declarations (Constants and Variables):

    define('MAX_COLS', '2');

    $conn = new Database();
    $hca_cont = new hca_controler();

    $response = "";
    $cons_hc = 0;
    $cnshca = "";
    $readonly = "";
    $disabled = "";
    $ateid = "";

    $afi = new Afiliado();
    $autofocus = '';


// Validations:

    if (!isset($_GET['hcpid'])) {
		
		// Creation:

        if (!isset($_GET['plahcid'])){
            $response = '<div class="alert alert-danger" role="alert"><strong>Error: Clase de Plantilla no válida</strong></div>'; // nuevo

            $data = array(
                "response" => $response,
                "status"   => "error",
                "id"       => 0
            );
            
            echo json_encode($data); // RETORNA UN ERROR

            exit;
        }

        if (!isset($_GET['pacid'])){
            $response = '<div class="alert alert-danger" role="alert"><strong>Id. de Paciente no válido</strong></div>';
            
            $data = array(
                "response" => $response,
                "status"   => "error",
                "id"       => 0
            );
            
            echo json_encode($data);
            exit;
        }

        $plantilla = $_GET['plahcid']; 								// CLASEPLANTILLA
        $id_afiliado = $_GET['pacid']; 								// IDAFILIADO
        $ateid = $_GET['ateid']; 									// ATEID
        $nombre_host = gethostbyaddr($_SERVER['REMOTE_ADDR']); 		// <-- SYS COMPUETER NAME
        $edadAfi = $afi->get_EdadAnios($id_afiliado); 				// EDAD DEL AFILIADO
        $documentepac = $afi->get_nodocumentoafi($id_afiliado); 	// NO DE DOCUMENTO DEL PACIENTE
        
        // Historia Clínica Nueva ... (MEJORAR EL TRABAJO PARAMETRO IDSEDE)
		
		// To Do: La Compañía y la Sede deben ser Variables de Entorno
		
        $cnshca = '01' . str_pad(fn_genConsecutivo("01", "01", "@HC", "", $conn), 8, "0", STR_PAD_LEFT); // <-- 1 - Se genera un nuevo consecutivo

        if(isset($cnshca)) {

            $h_clinica = [
                "saas_ciaid"         => 1,
                "consecutivo"        => $cnshca,
                "noadmision"         => "0100000001",       // (Parámetro) [Al ser CE no existe No. Admisión]
                "pacid"              => $id_afiliado,
                "idmedico"           => "1003045377",       // (Parámetro) [Este datos se setea como variable de entorno en el login]
                "afittodid"          => 0,
                "plahcid"            => $plantilla,
                "IDMEDICODILIGENCIA" => "1067874684",       // (Parámetro) [Este datos se setea como variable de entorno en el login]
                "edad"               => $edadAfi,
                "IDAREA"             => "05",               // (Parámetro) [Este datos se setea como variable de entorno en el login]
                "SYS_ComputerName"   => $nombre_host,
                "PROCEDENCIA"        => "CE",               // (Parámetro) [Siempre será CE]
                "GLASGOW"            => "Alerta",
                "CLASE"              => "HC",
                "ESTADO"             => "Activa",
                "IDSEDE"             => "01",               // (Parámetro) [Este datos se setea como variable de entorno en el login]
                "ateid"              => $ateid,
                "impreso"            => 0,
                "nodocumento"        => $documentepac
            ];

            // Creación de la HC: ( ANTES DE DIBUJAR LA INTERFAZ SE DEBE CREAR EL REGISTRO DE HISTORIA CLINICA )
            if ($hca_cont->fn_crear_registro_hc($conn, $h_clinica) == true) {
                
                $dataHc = [
                    "CONSECUTIVO"    => $cnshca,
                    "CLASEPLANTILLA" => $plantilla,
                    "IDAFILIADO"     => $id_afiliado
                ];
                
                if($hca_cont->fn_crear_camposHc($conn, $dataHc) == false) {
                    $response  = '<div class="alert alert-danger" role="alert"><strong>Error creando campos de la ';
                    $response .= 'Historia Clínica No.' . $cnshca . '</strong></div>';

                    $data = array(
                        "response" => $response,
                        "status"   => "error",
                        "id"       => 0
                    );

                    echo json_encode($data);
                    exit;
                } 

            } else {
                $response = '<div class="alert alert-danger" role="alert"><strong>Error de creación de Historia Clínica No.' . $cnshca . '</strong></div>';

                $data = array(
                    "response" => $response,
                    "status"   => "error",
                    "id"       => 0
                );

                echo json_encode($data);
                exit;
            }
            
            // To Do: Se neceaitan:  el id de médico (Variables de Entorno)

        } else {
			// NO SE GENERA EL CONSECUTIVO DEL REGISTRO DE HISTORIA CLINICA
			
            $response = '<div class="alert alert-danger" role="alert"><strong>Error: Consecutivo no generado!</strong></div>';
            
            $data = array(
                "response" => $response,
                "status"   => "error",
                "id"       => 0
            );
            
            echo json_encode($data);
            exit;
        }

    } else {
		
		// Modification:
		
        $cnshca = $_GET['hcpid'];                    		// <-- Campo CONSECUTIVO DE LA TABLA HCA
        $id_plt = $afi->get_ClasePlantilla($cnshca); 		// <-- CLASE DE PLANTILLA A PARTIR DEL CONSECUTIVO

        if (isset($_GET['accion'])){
            
            if ($_GET['accion'] == 'view') {
                $readonly = "readonly"; 
                $disabled = "disabled";
            } else {
                if ($_GET['accion'] == 'edit') {
                    // $fechaHistoria = $afi->get_fechaHc($cnshca);
                    // $fechaActual   = fn_getFechaMotor($conn);
                    // $intervaloDif  = fn_minutos_transcurridos($fechaHistoria, $fechaActual);
                    $lapsoModifica = $afi->get_LAPSOMODIFICAHC($id_plt);

                    // print('Variables ' . $fechaHistoria . " " . $fechaActual . " " . $intervaloDif . " " . $lapsoModifica . " " . intval($intervaloDif));
                    /*
                    if ($lapsoModifica == 0) {
                        $readonly = "readonly"; 
                        $disabled = "disabled";
                    } else {

                        if ($lapsoModifica == 1) {
                            $readonly = ""; 
                            $disabled = "";
                        }  else {
                            if ($lapsoModifica >= intval($intervaloDif)) {
                                $readonly = ""; 
                                $disabled = "";
                            } else {
                                $readonly = "readonly"; 
                                $disabled = "disabled";
                            }
                        }    
                    }*/
                    
                    switch ($lapsoModifica) {
                        case 0:
                            $readonly = "readonly"; 
                            $disabled = "disabled";
                            break;
                        case 1:
                            $readonly = ""; 
                            $disabled = "";
                            break;
                        default:
                            $fechaHistoria = $afi->get_fechaHc($cnshca);
                            $fechaActual   = fn_getFechaMotor($conn);
                            $intervaloDif  = fn_minutos_transcurridos($fechaHistoria, $fechaActual);

                            if ($lapsoModifica >= intval($intervaloDif)) {
                                $readonly = ""; 
                                $disabled = "";
                            } else {
                                $readonly = "readonly"; 
                                $disabled = "disabled";
                            }
                    }

                } else {
                    $readonly = ""; 
                    $disabled = "";
                }
            }

        } else {
            $readonly = ""; 
            $disabled = "";
        }
    }
	

    $plantilla = $afi->get_Plantilla($cnshca);  // <-- Obtiene el nombre de la Plantilla

    // Consulta para tarer campos de la historia:
    $consulta = "SELECT CONSECUTIVO, HCAD.CLASEPLANTILLA, HCAD.CAMPO, HCAD.SECUENCIA, NADA, NORMAL, ANORMAL, DETALLAR, NOTAS, TIPOCAMPO,
        ALFANUMERICO, CONVERT(VARCHAR(10),FECHA,126) AS FECHA, NUMERICO, MEMO, OBS, LISTA, ENEPICRISIS, HCADID, MPLD.DESCCAMPO, MPLD.SEXO,
        MPLD.EDADINI, MPLD.EDADFIN, MPLD.REQUERIDO, MPLD.ARRASTRAINFO, MPLD.TIPOEDICION, MPLD.TIPOARRASTRE, MPLD.DEFAULT1, MPLD.IMPRIMIR
        FROM HCAD WITH(INDEX(HCADCONSECUTIVO))
        INNER JOIN MPLD WITH(INDEX(MPLDCLASEPLANSECU)) ON MPLD.CLASEPLANTILLA = HCAD.CLASEPLANTILLA AND MPLD.SECUENCIA = HCAD.SECUENCIA 
        WHERE CONSECUTIVO = '" . $cnshca . "' ORDER BY HCAD.CONSECUTIVO,HCAD.SECUENCIA";

    $sth = $conn->prepare($consulta);
    $sth->execute();
    $result = $sth->fetchall(PDO::FETCH_ASSOC);

    // Consulta para traer los valores de lista:
    // $sqlLista = "SELECT x.hcpdlid, x.plahcdid, x.valorlista, x.checkm, x.hcpdid FROM hcpdl x WHERE x.hcpid = $cons_hc";
    $sqlLista = "SELECT CAMPO,VALORLISTA FROM MPLDL WHERE CLASEPLANTILLA='" . $afi->get_ClasePlantilla($cnshca) . "' ";

    $sthLista = $conn->prepare($sqlLista);
    $sthLista->execute();
    $resultLista = $sthLista->fetchall(PDO::FETCH_ASSOC);

    $sqlValoresLista = "SELECT 
		HCADL.CONSECUTIVO,
		HCADL.SECUENCIA,
		HCADL.VALORLISTA,
		HCADL.CHECKM,
		HCADL.DESCRIPCION,
		HCADL.ITEM,
		HCADL.HCADLID,
		HCADL.MPLDLID
	FROM 
		HCADL
	WHERE 
		HCADL.CONSECUTIVO='{$cnshca}' ORDER BY HCADL.MPLDLID";

    $sthValoresLista = $conn->prepare($sqlValoresLista);
    $sthValoresLista->execute();
    $valoresLista = $sthValoresLista->fetchall(PDO::FETCH_ASSOC);

    // Creación de la Interfaz Gráfica de la Historia Clínica:
    $response = "";
    
    if ($readonly == "readonly") {
        $msgAdd = '<h6>(Historia Cl&iacute;nica de solo lectura)</h6>';
    } else {
        $msgAdd = '';
    }

    //  SE ARMA LA VISTA DE LA PLANTILLA DE HISTORIA CLINICA PARA EL USUARIO  			// $response .= '<div class="container border border-primary rounded">';
    
    $response .= '<br><div align="center" class="p-2 bg-info text-white"><h4>' . $plantilla . '</h4> ' . $msgAdd . '</div><br>';
    $response .= '<div class="row">';
    $response .= '<div class="col-md-10 mx-auto">';
    $response .= '<form role="form" id="hca_form">';
        
        $campos = $sth->rowCount();
        $i = 0;
        $rows = 0;
        $tipocampo = "";
        $selected = "";

        foreach ($result as $row) {

            $tipocampo      = $row['TIPOCAMPO'];    	// <-- Tipo del campo de Plantilla
            $campo          = $row['HCADID'];           // <-- identificador del campo de Plantilla
            $desccampo      = $row['DESCCAMPO'];        // <-- Descripción del campo '<strong>' . $row['desccampo'] . '</strong>';
            $secuencia      = $row['SECUENCIA'];        // <-- Secuencia del campo (consecutivo dentro de la Plantilla)
            $alfanumerico   = $row['ALFANUMERICO'];     // <-- Valor en columna alfanumérico   
            $fecha          = $row['FECHA'];            // <-- Valor en columna Fecha
            $memo           = $row['MEMO'];             // <-- Valor en columna Memo
            $numerico       = $row['NUMERICO'];         // <-- Valor en columna Numérico
            $claseplantilla = $row['CLASEPLANTILLA'];   // <-- Identificador de la Plantilla de H. Clínica
            $campoHc        = $row['CAMPO'];           	// <-- Identificador del campo en la Palntilla de H. Clínica   
            $a              = 'autofocus';
            $idvalorlista   = 0;

            // $i se refiere al número de columnas que se van a usar (se trabaja junto con la constante MAX_COLS)

            if (rtrim(ltrim($tipocampo)) != 'Titulo' && rtrim(ltrim($tipocampo)) != 'Subtitulo') {
                if ($i == 0) {
                    $response .= '<div class="form-group row">';
                }
            } else {
                if ($i != 0) {
                    $response .= '</div>';
                    $i=0;
                }
            }

            switch (rtrim(ltrim($tipocampo))) {
                case "Titulo":
                    {
                        $response .= '<div class="p-2 bg-info text-white" name="STC_'.$campo.'_'.$secuencia.'" id=STC_'.$campo.'_'.$secuencia.'">'.$desccampo.'</div>';
                        break;
                    }
                case "Subtitulo":
                    {
                        $response .= '<div class="p-2 bg-warning text-white" name="STC_'.$campo.'_'.$secuencia.'" id=STC_'.$campo.'_'.$secuencia.'">'.$desccampo.'</div>';
                        break;
                    }
                // case "TituloN3":
                //     {
                //         $response .= '<div class="p-2 bg-success text-white" name="STC_'.$campo.'_'.$secuencia.'" id=STC_'.$campo.'_'.$secuencia.'">'.$desccampo.'</div>';
                //         break;
                //     }    
                case "Alfanumerico":
                    {    
                        $response .= '<div class="col">';
                        $response .= '<label>'.$desccampo.'</label>';
                        $response .= '<input type="text" name="EDT_'.$campo.'_'.$secuencia.'" id="EDT_'.$campo.'_'.$secuencia.'" value="'.$alfanumerico.'" size="50" class="form-control" onblur="save_valor(this.id,\''.$tipocampo.'\',\''.$cnshca.'\')" ';
                        $response .= ' onfocus="ingreso_campo(this.id)" '.$readonly.'>';
                        $response .= '</div>'; // '.$a.' 
                        break;
                    }
                case "Numerico":
                    {    
                        $response .= '<div class="col">';
                        $response .= '<label>'.$desccampo.'</label>';
                        $response .= '<input type="number" name="EDT_'.$campo.'_'.$secuencia.'" id="EDT_'.$campo.'_'.$secuencia.'" value="'.$numerico.'" size="50" class="form-control" onblur="save_valor(this.id,\''.$tipocampo.'\',\''.$cnshca.'\')" ';
                        $response .= ' onfocus="ingreso_campo(this.id)" '.$readonly.'>';
                        $response .= '</div>'; // '.$a.' 
                        break;
                    }    
                case "Memo":
                    {
                        $response .= '<div class="col">';
                        $response .='<label>'.$desccampo.'</label>';
                        $response .= '<textarea name="TEXT_'.$campo.'_'.$secuencia.'" id="TEXT_'.$campo.'_'.$secuencia.'" cols="60" rows="4" class="form-control" '; // >'.$memo.'</textarea>';
                        $response .= 'onfocus="ingreso_campo(this.id)" ';
                        $response .= 'onblur="save_valor(this.id,\''.$tipocampo.'\',\''.$cnshca.'\')" '.$readonly.'>';
                        $response .= ''.$memo.'</textarea>'; 
                        $response .= '</div>'; //'.$a.' 
                        break;
                    } 
                case "Fecha":
                    {
                        // value= ?PHP  echo date('Y-m-d',strtotime($fecha ));  ?
                        $response .= '<div class="col">';
                        $response .= '<label>'.$desccampo.'</label>';
                        $response .= '<input type="date" name="EDT_'.$campo.'_'.$secuencia.'" id="EDT_'.$campo.'_'.$secuencia.'" value="'.date('Y-m-d',strtotime($fecha)).'" class="form-control" ';
                        $response .= 'onfocus="ingreso_campo(this.id)" ';
                        $response .= 'onblur="save_valor(this.id,\''.$tipocampo.'\',\''.$cnshca.'\')" '.$readonly.'>'; //.$fecha.           // echo 'Fecha: ' . date('Y-m-d',strtotime($fecha));
                        $response .= '</div>'; // '.$a.' 
                        break;
                    }
                // case "Hora":
                //     {
                //         $response .= '<div class="col">';
                //             $response .= '<label>'.$desccampo.'</label>';
                //             $response .= '<input type="time" name="EDT_'.$campo.'_'.$secuencia.'" id="EDT_'.$campo.'_'.$secuencia.'" value="'.date('H:i',strtotime($fecha)).'" class="form-control" ';
                //             $response .= 'onfocus="ingreso_campo(this.id)" ';
                //             $response .= 'onblur="save_valor(this.id,\''.$tipocampo.'\',\''.$cons_hc.'\')" '.$readonly.'>';
                //             // echo 'Hora: ' . $fecha;
                //         $response .= '</div>'; // '.$a.' 
                //         break;
                //     }
                // case "Fechahora":
                //     {
                //         $response .= '<div class="col">';
                //             $response .= '<label>'.$desccampo.'</label>';
                //             // echo '<input type="datetime-local" '.$a.' name="EDT_'.$campo.'_'.$secuencia.'" id="EDT_'.$campo.'_'.$secuencia.'" value="'.$fecha.'" class="form-control" ';
                //             $response .= '<input type="datetime-local" name="EDT_'.$campo.'_'.$secuencia.'" id="EDT_'.$campo.'_'.$secuencia.'" value="'.date('Y-m-d',strtotime($fecha)).'T'.date('H:i',strtotime($fecha)).'" class="form-control" ';
                //             // date_format($date, 'Y-m-d H:i:s');
                //             $response .= 'onfocus="ingreso_campo(this.id)" ';
                //             $response .= 'onblur="save_valor(this.id,\''.$tipocampo.'\',\''.$cons_hc.'\')" '.$readonly.'>';
                //             // echo 'Fecha: ' . date('Y-m-d H:i:s',strtotime($fecha));
                //         $response .= '</div>'; // '.$a.' 
                //         break;
                //     }        
                case "Lista":
                    {
                        $response .= '<div class="col">';
                            $response .= '<label>'.$desccampo.'</label>';
                            $response .= '<select name="COMBO_'.$campo.'_'.$secuencia.'" class="form-control" id="COMBO_'.$campo.'_'.$secuencia.'" ';
                            $response .= 'onChange="save_valor(this.id,\''.$tipocampo.'\',\''.$cnshca.'\')" '.$readonly.'>';
                            
                                $response .= '<option value=""></option>';
                                foreach ($resultLista as $rowx) {
                                    if ($rowx['CAMPO'] == $campoHc) {
                                        if ($rowx['VALORLISTA'] == $alfanumerico) {
                                            $sel = 'selected="selected"';
                                        } else {
                                            $sel = '';
                                        }

                                        // [ Averiguar con Fredy si se puede agregar el campo VALOR_GUARDAR a los campos de tipo [Lista] ]
                                        $response .= '<option value="'.$rowx['VALORLISTA'].'" '.$sel.' >'.$rowx['VALORLISTA'].'</option>';
                                    }
                                }

                            $response .= '</select>';
                        $response .= '</div>';

                        break;
                    }
                case "MultiCheck":
                    {
                        // SELECT x.hcpdlid, x.plahcdid, x.valorlista, x.checkm, x.hcpdid FROM hcpdl x WHERE x.hcpid = $cons_hc
                        $response .= '<div class="col">';
                            foreach ($valoresLista as $rowx) {
                                if ($rowx['SECUENCIA'] == $secuencia) {

                                    if ($rowx['CHECKM'] == 1) {
                                        $selected = "CHECKM";
                                    } else {
                                        $selected = "";
                                    }

                                    $idvalorlista = $rowx['HCADLID'];

                                    $response .= '<input type="checkbox" name="CBOX_'.$campo.'_'.$secuencia.'_'.$idvalorlista.'" id="CBOX_'.$campo.'_'.$secuencia.'_'.$idvalorlista.'" value="CBOX_'.$campo.'_'.$secuencia.'_'.$idvalorlista.'" ';
                                    $response .= 'onChange="save_valor(this.id,\''.$tipocampo.'\',\''.$cnshca.'\')" ' . $selected . ' ' . $readonly . ' ' . $disabled . '>';
                                    $response .= '<label for="CBOX_'.$campo.'_'.$secuencia.'_'.$idvalorlista.'" style="display: inline;"> '.$rowx['VALORLISTA'].'</label>'; //
                                    $response .= '<br>';
                                }
                            }
                        $response .= '</div>';
                        break;            
                    }    
            }

            $rows++;

            if (rtrim(ltrim($tipocampo)) != 'Titulo' && rtrim(ltrim($tipocampo)) != 'Subtitulo') {
                $i++;
            }
            
            if ($i == MAX_COLS) {                       // <-- Si ya van dos columnas cortar el form group row
                $response .= '</div>';
                $i = 0;
            }

            if ($rows == $campos) {
                $response .= '</div>';
            }

            if ($autofocus == '') {
                $autofocus = 'Y';
                $a = '';
            }

        }

        $response .= '<div class="row">';
            $response .= '<div class="col">&nbsp;</div>';
        $response .= '</div>';

        $response .= '<br><br>';
        $response .= '</form>';
    $response .= '</div></div>';

    $data = array(
        "response" => $response,
        "status"   => "success",
        "id"       => $cnshca
    );
    
    echo json_encode($data); // nuevo
?>
