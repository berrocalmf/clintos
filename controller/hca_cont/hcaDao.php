<?php
    require_once '../../database.class.php';
    require_once '../../model/hca/hca_mod.php';
    // require_once($_SERVER['DOCUMENT_ROOT']."/database.class.php");
    
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        // {hcpdid: id_campo, tipo_campo: tipo_campo, valor_campo: valor_campo, hcpid: consecutivo}
        if(!isset($_POST['hcpdid']) || !isset($_POST['tipo_campo']) || !isset($_POST['valor_campo']) || !isset($_POST['hcpid'])) {
            echo '<h6>No se ejecuta nada</h6>';
            return;
        }

        $hcpdid      = $_POST['hcpdid'];
        $tipo_campo  = $_POST['tipo_campo'];
        $valor_campo = $_POST['valor_campo'];
        $hcpid       = $_POST['hcpid'];

        //  data: {hcpdid: id_campo, tipo_campo: tipo_campo, valor_campo: valor_campo, hcpid: consecutivo},
        // echo 'id. Campo = ['.$id_campo.'] - Tipo = ['.$tipo_campo.'] - Valor: ['.$valor_campo.'] - consecutivo = ['.$consecutivo.']';
        
        try {
            $c     = new Database();
            $hca_c = new hca_mod();
            $hca_c->salvarCampo($c, $hcpdid, $tipo_campo, $valor_campo, $hcpid);
        } catch(Exception $ex) {
			echo $ex;
        }
    } else {
        echo 'No ingresa a este método';
    }

?>