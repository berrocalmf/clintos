<?php 
    header("Content-type: application/json");

    // Controlador de MDX (MdxController)

    // Se establece conexión
    include_once "database.class.php";
    $conn = new Database();

    // Se capturan los parametros
    $descdx = isset($_GET['descdx']) ? $_GET['descdx'] : '';

    // Se arma el query que se lanzara a la base de datos y se ejecuta la consulta a la base de datos
    $consulta = "SELECT x.IDDX as mdxid, x.IDDX as coddx, x.DESCRIPCION as nomdx FROM MDX x WHERE x.DESCRIPCION LIKE '%" . $descdx . "%'";
    $sth    = $conn->prepare($consulta);
    $sth->execute();
    $result = $sth->fetchall(PDO::FETCH_ASSOC);

    // Retornamos un objeto json
    echo json_encode($result);
?>
